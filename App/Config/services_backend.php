<?php

declare(strict_types=1);

use Phalcon\Container;
use Phalcon\Mvc\Router;
use Phalcon\Url as UrlResolver;

/**
 * @var Container $container
 */

/**
 * The URL component is used to generate all kind of urls in the application
 */
$container->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
});

$container->setShared(
    'router',
    function () {
        $router = new Router();
        $router->removeExtraSlashes(true);
        return $router;
    }
);
