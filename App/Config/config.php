<?php

/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */

use Dotenv\Dotenv;
use Phalcon\Config;

defined('BASE_PATH')
|| define(
    'BASE_PATH',
    getenv('BASE_PATH') ?: realpath(dirname(__FILE__).'/../..')
);
defined('APP_PATH') || define('APP_PATH', BASE_PATH.'/App');

require_once(BASE_PATH.'/vendor/autoload.php');
Dotenv::createImmutable(BASE_PATH)->load();

return new Config([
    'database' => [
        'adapter'  => $_ENV['DB_TYPE'],
        'host'     => $_ENV['DB_HOST'],
        'username' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PASS'],
        'dbname'   => $_ENV['DB_NAME'],
        'charset'  => 'utf8',
    ],

    'application' => [
        'controllersDir' => APP_PATH.'/Http/Controllers/',
        'modelsDir'      => APP_PATH.'/Entities/',
        'baseUri'        => '/',
        'debug'          => $_ENV['DEBUG'],
    ],

    'session' => [
        'path'          => APP_PATH.'/Storage/tmp',
        'table'         => 'sessions', // Not used currently, when we switch to DB adapter
        'cookie_params' => [
            'lifetime' => intval($_ENV['SESSION_LIFETIME']),
            'domain'   => $_ENV['SESSION_DOMAIN'],
            'secure'   => $_ENV['SESSION_SECURE'],
            'httponly' => $_ENV['SESSION_HTTP_ONLY'],
            'samesite' => $_ENV['SESSION_SAME_SITE'],
        ],
    ],

    'logging' => [
        'general'  => 'errors.log',
        'database' => 'query.log',
    ],
]);
