<?php

declare(strict_types=1);

/**
 * Registering an autoloader
 */

defined('BASE_PATH')
|| define(
    'BASE_PATH',
    getenv('BASE_PATH') ?: realpath(dirname(__FILE__))
);
defined('APP_PATH') || define('APP_PATH', BASE_PATH.'/App');

use Dotenv\Dotenv;
use Phalcon\Loader;

$loader = new Loader();

// Register App namespaces
//$loader->registerNamespaces(
//    [
//        'App\Enums'                      => APP_PATH.'/Enums/',
//        'App\Http\Controllers'           => APP_PATH.'/Http/Controllers/',
//        'App\Http\Resources'             => APP_PATH.'/Http/Resources/',
//        'App\Http\Resources\Serializers' => APP_PATH.'/Http/Resources/Serializers',
//        'App\Http\Responses'             => APP_PATH.'/Http/Responses/',
//        'App\Http\Transformers'          => APP_PATH.'/Http/Transformers/',
//        'App\Http\Validators'            => APP_PATH.'/Http/Validators/',
//        'App\Http\Middleware'            => APP_PATH.'/Http/Middleware/',
//        'App\Entities'                   => APP_PATH.'/Entities/',
//        'App\Models'                     => APP_PATH.'/Models/',
//        'App\Helpers'                    => APP_PATH.'/Helpers/',
//        'App\Services'                   => APP_PATH.'/Services/',
//        'App\Providers'                  => APP_PATH.'/Providers/',
//        'App\Repositories'               => APP_PATH.'/Repositories/',
//        'App\Traits'                     => APP_PATH.'/Traits/',
//        'App\Storage\Adapters'           => APP_PATH.'/Storage/Adapters/',
//        'App\Cli'                        => APP_PATH.'/Cli/',
//    ]
//);

$loader->registerDirs([
    APP_PATH.'/Cli',
    APP_PATH.'/Entities',
    APP_PATH.'/Enums',
    APP_PATH.'/Exceptions',
    APP_PATH.'/Services',
]);

$loader->registerNamespaces([
    'App\Cli' => __DIR__,
]);

$loader->register();

// Load environment variables
Dotenv::createImmutable(getcwd())->load();
