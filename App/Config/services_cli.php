<?php

declare(strict_types=1);

use Phalcon\Cli\Dispatcher;
use Phalcon\Container;

/**
 * @var Container $container
 */

/**
 * Set the default namespace for dispatcher
 */
$container->setShared('dispatcher', function () {
    $dispatcher = new Dispatcher();
    $dispatcher->setDefaultNamespace('App\Cli');
    $dispatcher->setNamespaceName('App\Cli');
    return $dispatcher;
});
