<?php

declare(strict_types=1);

use App\Enums\GlobalServicesEnum;
use Dotenv\Dotenv;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Monolog\Processor\PsrLogMessageProcessor;
use Phalcon\Config;
use Phalcon\Db\Adapter\AbstractAdapter;
use Phalcon\Di\DiInterface;
use Phalcon\Events\Event;
use Phalcon\Session\Adapter\Stream;
use Phalcon\Session\Manager;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * @var DiInterface $container
 */

$container->setShared(
    'env',
    function () {
        return Dotenv::createImmutable(__DIR__."/..");
    }
);

/**
 * Shared configuration service
 */
$container->setShared('config', function () {
    return include APP_PATH."/Config/config.php";
});

$container->setShared(
    GlobalServicesEnum::LOGGER,
    function () use ($container) {
        /** @var Config $config */
        $config = $container->get('config');

        $generalLogName = $config->path('logging.general');
        $generalLogPath = APP_PATH.'/Storage/Logs';
        $generalLogFile = $generalLogPath.'/'.$generalLogName;

        $generalLogFormatter = new LineFormatter(
            "[%datetime%][%level_name%] %message%\n\t [%context%]\n"
        );

        $generalLogger = new Logger('logger');

        $streamHandler = new RotatingFileHandler(
            $generalLogFile, Logger::DEBUG
        );
        $streamHandler->setFormatter($generalLogFormatter);

        $psrProcessor = new PsrLogMessageProcessor();

        $generalLogger->pushHandler($streamHandler);
        $generalLogger->pushProcessor($psrProcessor);

//        $dbLogName = $config->path('logging.database');
//        $dbLogPath = APP_PATH . '/Storage/Logs';
//        $dbLogFile = $dbLogPath . '/' . $dbLogName;
//
//        $formatter = new LineFormatter("[%datetime%][DEBUG] %message%\n\t %variables%\n");
//
//        $dbLogger = new Logger('db-logger');
//
//        $dbLogStreamHandler = new RotatingFileHandler($dbLogFile, Logger::DEBUG);
//        $dbLogStreamHandler->setFormatter($formatter);
//
//        $dbLogger->pushHandler($dbLogStreamHandler);

        return $generalLogger;
    }
);

$container->setShared(
    'events-manager',
    function () {
        return new \Phalcon\Events\Manager();
    }
);

/**
 * Database connection is created based on the parameters defined in the configuration file
 */
$container->setShared(GlobalServicesEnum::DB, function () use ($container) {
    $config = $this->getConfig();

    $class = 'Phalcon\Db\Adapter\Pdo\\'.$config->database->adapter;
    $params = [
        'host'     => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'charset'  => $config->database->charset,
    ];

    if ($config->database->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    /** @var AbstractAdapter $connection */
    $connection = new $class($params);

    // Log queries
    if ($_ENV['DEBUG'] === 'true') {
        /** @var \Phalcon\Events\Manager $eventManager */
        $eventManager = $container->get('events-manager');
        $eventManager->attach(
            'db:beforeQuery',
            function (Event $event, AbstractAdapter $connection) use ($container
            ) {
                /** @var Logger $logger */
                $logger = $container->getShared('logger');
                $logger->withName('db-logger')->debug(
                    $connection->getSQLStatement(),
                    ['variables' => json_encode($connection->getSqlVariables())]
                );
            }
        );
        $connection->setEventsManager($eventManager);
    }

    return $connection;
});

$container->set(
    GlobalServicesEnum::SESSION,
    function () use ($container) {
        $config = $this->getConfig();
        $session = new Manager();
        $files = new Stream(
            [
                'savePath' => $config->session->path,
            ]
        );
//        $db = new SessionDatabaseAdapter($container->getShared(GlobalServicesEnum::DB));

        $session->setAdapter($files);

//        if ($session->exists()) {
//            $session->start();
//        } else {
//        }
//        $sessionParams = [
//            'lifetime' => $config->path('session.cookie_params.lifetime'),
//            // 'domain' => $config->path('session.cookie_params.domain'),
//            'secure' => $config->path('session.cookie_params.secure'),
//            'httponly' => $config->path('session.cookie_params.httponly'),
//            'samesite' => $config->path('session.cookie_params.samesite'),
//        ];
//        session_set_cookie_params($sessionParams);
        if ( ! $session->exists()) {
            ini_set('session.cookie_samesite', 'None');
            ini_set('session.cookie_secure', 'true');
        }
        $session->start();

        return $session;
    }
);

$container->setShared(
    GlobalServicesEnum::EMAIL,
    function () {
        //Server settings
        $phpmailer = new PHPMailer();
        $phpmailer->isSMTP();
        $phpmailer->Host = $_ENV['SMTP_HOST'];
        $phpmailer->SMTPAuth = true;
        $phpmailer->SMTPSecure = 'tls';
        $phpmailer->Port = $_ENV['SMTP_PORT'];
        $phpmailer->Username = $_ENV['SMTP_USER'];
        $phpmailer->Password = $_ENV['SMTP_PASS'];
        $phpmailer->setFrom($_ENV['SMTP_FROM']);
        $phpmailer->isHTML(true);

        return $phpmailer;
    }
);

$container->setShared(
    GlobalServicesEnum::SECURITY,
    function () {
        $security = new \Phalcon\Security();

        $security->setDefaultHash(\Phalcon\Security::CRYPT_SHA256);

        return $security;
    }
);

// $container->set(
//     'modelsMetadata',
//     function () {
//         $options = [
//             'prefix'      => $_ENV['ENV'],
//             'metaDataDir' => APP_PATH.'/Storage/Cache/MetaData',
//         ];

//         return new Stream($options);
//     }
// );

//$container->set(
//    'sessionBag',
//    function () {
//        $bag = new \Phalcon\Session\Bag('data');
//        $bag->init();
//
//        return $bag;
//    }
//);

//(new ResponseProvider())->register($container);
