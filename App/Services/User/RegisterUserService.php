<?php

namespace App\Services\User;

use App\Entities\Users;
use App\Repositories\UserRepository;
use App\Repositories\VerificationRepository;
use App\Services\Mail\SendMailService;
use Phalcon\Di\DiInterface;
use Phalcon\Di\FactoryDefault;
use Phalcon\Security;

class RegisterUserService
{
    private DiInterface $container;
    private UserRepository $userRepository;
    private VerificationRepository $verificationRepository;
    private SendMailService $sendMailService;

    public function __construct()
    {
        $this->container = FactoryDefault::getDefault();
        $this->userRepository = new UserRepository();
        $this->verificationRepository = new VerificationRepository();
        $this->sendMailService = new SendMailService();
    }

    public function handle(array $parameters): Users
    {
        $security = new Security();

        $encryptedPassword = $security->hash($parameters['password']);
        $parameters['password'] = $encryptedPassword;

        $user = $this->userRepository->register($parameters);

        $verification = $this->verificationRepository->create($user->getId());

        $this->sendMailService->handle(
            "Please verify your Commune account",
            [$user->getEmail()],
            'user_verify',
            [
                'user' => $user->getUsername(),
                'token' => $verification->getToken(),
            ]
        );

        return $user;
    }
}
