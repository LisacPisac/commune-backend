<?php

namespace App\Services\User;

use App\Entities\Users;
use App\Repositories\UserRepository;
use App\Services\Auth\LogoutService;
use Phalcon\Di\FactoryDefault;

class DeleteUserService
{
    private UserRepository $userRepository;
    private LogoutService $logoutService;

    function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->logoutService = new LogoutService(FactoryDefault::getDefault());
    }

    public function handle(Users $user)
    {
        $this->userRepository->delete($user);

        $this->logoutService->handle();

        return true;
    }
}
