<?php

namespace App\Services\User;

use App\Repositories\UserRepository;

class GetUserService
{
    private UserRepository $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function handle(array $parameters)
    {
        return $this->userRepository->findById(intval($parameters['user_id']));
    }
}