<?php

namespace App\Services\User;

use App\Entities\Files;
use App\Entities\Users;
use App\Exceptions\ValidationException;
use App\Repositories\UserRepository;
use App\Repositories\VerificationRepository;
use App\Services\FileHandlerService;
use App\Services\Mail\SendMailService;
use Phalcon\Http\Request\File;
use Phalcon\Messages\Message;
use Phalcon\Messages\Messages;

class UpdateUserService
{
    private UserRepository $userRepository;
    private FileHandlerService $fileHandlerService;
    private VerificationRepository $verificationRepository;
    private SendMailService $sendMailService;
    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->fileHandlerService = new FileHandlerService();
        $this->verificationRepository = new VerificationRepository();
        $this->sendMailService = new SendMailService();
    }

    public function handle(
        Users $user,
        array $parameters,
        array $files = []
    ): Users {
        $preparedParameters = $this->prepareParameters($parameters);

        if ( ! empty($preparedParameters['email']) && $preparedParameters['email'] !== $user->getEmail()) {
            $verification = $this->verificationRepository->create($user->getId());

            $this->sendMailService->handle(
                "Please verify your Commune account",
                [$preparedParameters['email']],
                'user_verify',
                [
                    'user'  => $user->getUsername(),
                    'token' => $verification->getToken(),
                ]
            );
        }

        $user = $this->userRepository->update($user, $preparedParameters);


        if ( ! empty($files)) {
            /** @var File $file */
            $file = $files[0];

            if ($user->isPremium()) {
                if ($file->getSize() > Files::FILE_SIZE_LIMIT_PREMIUM) {
                    $messages = new Messages();
                    $messages->appendMessage(new Message('Some of your files are too big', 'avatar'));
                    throw new ValidationException($messages);
                }
            } else {
                if ($file->getSize() > Files::FILE_SIZE_LIMIT) {
                    $messages = new Messages();
                    $messages->appendMessage(new Message('Some of your files are too big', 'avatar'));
                    throw new ValidationException($messages);
                }
            }

            if (mime_content_type($file->getTempName()) === 'image/gif' && ! $user->isPremium()) {
                $messages = new Messages();
                $messages->appendMessage((new Message('Only premium users can set animated avatars.', 'avatar')));

                throw new ValidationException($messages);
            }

            if (strpos(mime_content_type($file->getTempName()), "image") === false) {
                $messages = new Messages();
                $messages->appendMessage((new Message('Avatar must be an image.', 'avatar')));

                throw new ValidationException($messages);
            }

            $user->setAvatarFileId(
                $this->fileHandlerService->handle($file)->getId()
            )->save();
        }

        return $user->refresh();
    }

    private function prepareParameters(array $parameters)
    {
        $prepared = [];

        if (array_key_exists('email', $parameters) && ! empty($parameters['email'])) {
            $prepared['email'] = $parameters['email'];
        }

        if (array_key_exists('username', $parameters) && ! empty($parameters['username'])) {
            $prepared['username'] = $parameters['username'];
        }

        if (array_key_exists('subtitle', $parameters) && ! empty($parameters['subtitle'])) {
            $prepared['subtitle'] = $parameters['subtitle'];
        }

        if (array_key_exists('summary', $parameters) && ! empty($parameters['summary'])) {
            $prepared['summary'] = $parameters['summary'];
        }

        return $parameters;
    }
}
