<?php

namespace App\Services\User;

use App\Entities\Users;
use App\Exceptions\NotFoundException;
use App\Services\Auth\LogoutService;
use Phalcon\Di\FactoryDefault;

class GetProfileService
{
    private LogoutService $logoutService;

    public function __construct()
    {
        $this->logoutService = new LogoutService(FactoryDefault::getDefault());
    }

    public function handle(Users $user)
    {
        if (empty($user) || $user->getIsDeleted()) {
            $this->logoutService->handle();
            throw new NotFoundException();
        }

        return $user;
    }
}
