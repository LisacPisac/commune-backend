<?php

namespace App\Services\Mail;

use App\Enums\GlobalServicesEnum;
use Phalcon\Di\DiInterface;
use Phalcon\Di\FactoryDefault;
use PHPMailer\PHPMailer\PHPMailer;

class SendMailService
{
    private DiInterface $container;

    public function __construct()
    {
        $this->container = FactoryDefault::getDefault();
    }

    public function handle(string $subject, array $recipients, string $templateName, array $templateVars)
    {
        /** @var PHPMailer $mailer */
        $mailer = $this->container->get(GlobalServicesEnum::EMAIL);

        foreach ($recipients as $recipient) {
            $mailer->addAddress($recipient);
        }
        $mailer->Subject = $subject;
        $mailer->msgHTML($this->parseHtml($templateName, $templateVars));
        $mailer->send();
    }

    private function parseHtml(string $templateName, array $templateVars): string
    {
        $body = file_get_contents(APP_PATH . '/Storage/Mail/' . $templateName . '.html');

        foreach ($templateVars as $key => $var) {
            $body = preg_replace("/:$key:/", $var, $body);
        }

        return $body;
    }
}