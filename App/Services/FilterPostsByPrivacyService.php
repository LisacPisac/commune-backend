<?php

namespace App\Services;

use App\Entities\Contacts;
use App\Entities\Groups;
use App\Entities\Members;
use App\Entities\Posts;
use App\Entities\Users;
use App\Enums\PostPrivacySettingsEnum;
use App\Repositories\ContactRepository;
use Phalcon\Mvc\Model\ResultsetInterface;

class FilterPostsByPrivacyService
{
    private ContactRepository $contactRepository;

    public function __construct()
    {
        $this->contactRepository = new ContactRepository();
    }

    public function handle(ResultsetInterface $posts, Users $authUser)
    {
        $filteredPosts = $posts->filter(
            function (Posts $post) use ($authUser) {
                $canSeePost = true;

                if ($post->getAuthor() === $authUser->getId()) {
                    return $post;
                }

                if ($post->getPrivacy() === PostPrivacySettingsEnum::GROUP) {
                    /** @var Groups $group */
                    $group = $post->getGroups();

                    $members = $group->getMembers();

                    $isMember = false;
                    /** @var Members $member */
                    foreach ($members as $member) {
                        if ($member->getUserId() === $authUser->getId()) {
                            $isMember = true;
                            break;
                        }
                    }

                    if ( ! $isMember) {
                        $canSeePost = false;
                    }
                }

                if ($post->getPrivacy() === PostPrivacySettingsEnum::CONTACTS) {
                    $postAuthorContacts = $this->contactRepository->getForUser($post->getUsers()->getId());

                    $isContact = false;
                    /** @var Contacts $contact */
                    foreach ($postAuthorContacts as $contact) {
                        if ($contact->getId() === $authUser->getId()) {
                            $isContact = true;
                            break;
                        } else {
                            if ($contact->getId() === $authUser->getId()) {
                                $isContact = true;
                                break;
                            }
                        }
                    }

                    if ( ! $isContact) {
                        $canSeePost = false;
                    }
                }

                if ($canSeePost) {
                    return $post;
                }
            }
        );

        return $filteredPosts;
    }
}
