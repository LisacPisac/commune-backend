<?php

namespace App\Services;

use App\Entities\Users;
use App\Exceptions\NotFoundException;
use App\Exceptions\ValidationException;
use App\Repositories\UserRepository;
use Phalcon\Messages\Message;
use Phalcon\Messages\Messages;
use Phalcon\Security;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email;
use RobThree\Auth\TwoFactorAuth;

class LoginService
{
    private UserRepository $userRepository;
    private ?Users $user = null;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    /**
     * @throws ValidationException
     */
    public function handle(array $parameters): void
    {
        $this->validate($parameters);

        $user = $this->userRepository->findByEmail(
            $parameters['email']
        );

        if ( ! empty($user)) {
            $security = new Security();
            if ($security->checkHash($parameters['password'], $user->getPassword())) {
                if (array_key_exists('tfa_code', $parameters)) {
                    $tfa = new TwoFactorAuth();
                    $verify = $tfa->verifyCode($user->getTfaSecret(), $parameters['tfa_code']);

                    if ( ! $verify) {
                        $messages = new Messages();
                        $messages->appendMessage(new Message('Invalid code', 'tfa_code'));
                        throw new ValidationException($messages);
                    }
                }
                $this->user = $user;

                if ( ! $this->user->getEnabled()) {
                    $messages = new Messages();
                    $messages->appendMessage(new Message('User not enabled', 'email'));
                    throw new ValidationException($messages);
                }

                if ($this->user->getIsDeleted()) {
                    throw new NotFoundException();
                }
            }
        }
    }

    public function getUser(): ?Users
    {
        return $this->user;
    }

    public function validate(array $parameters)
    {
        $validator = new Validation();

        $validator->add(
            "email",
            new Email(
                [
                    "message" => "The e-mail is not valid",
                ]
            )
        );

        $messages = $validator->validate($parameters);

        if ( ! empty($messages->count())) {
            throw new ValidationException($messages);
        }
    }
}
