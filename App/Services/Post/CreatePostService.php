<?php

namespace App\Services\Post;

use App\Entities\Files;
use App\Entities\PostFiles;
use App\Entities\Posts;
use App\Entities\Users;
use App\Enums\GlobalServicesEnum;
use App\Exceptions\ValidationException;
use App\Repositories\PostRepository;
use Phalcon\Db\Adapter\AdapterInterface;
use Phalcon\Di\DiInterface;
use Phalcon\Di\FactoryDefault;
use Phalcon\Http\Request\File;
use Phalcon\Messages\Message;
use Phalcon\Messages\Messages;
use Throwable;

class CreatePostService
{
    private PostRepository $postRepository;
    private DiInterface $container;

    public function __construct()
    {
        $this->container = FactoryDefault::getDefault();
        $this->postRepository = new PostRepository();
    }

    public function handle(
        Users $authUser,
        array $parameters,
        array $files = []
    ): Posts {
        /** @var AdapterInterface $dbAdapter */
        $dbAdapter = $this->container->get(GlobalServicesEnum::DB);

        $dbAdapter->begin();

        try {
            if (empty($files) && empty($parameters['text'])) {
                $messages = new Messages();
                $messages->appendMessage(new Message('Please provide some content', 'text'));
                throw new ValidationException($messages);
            }

            $post = $this->postRepository->create(
                $authUser->getId(),
                $this->validateAndPrepareParamters($parameters)
            );

            if ( ! empty($files)) {
                /** @var File $file */
                foreach ($files as $file) {
                    if ($authUser->isPremium()) {
                        if ($file->getSize() > Files::FILE_SIZE_LIMIT_PREMIUM) {
                            $messages = new Messages();
                            $messages->appendMessage(new Message('Some of your files are too big', 'files'));
                            throw new ValidationException($messages);
                        }
                    } else {
                        if ($file->getSize() > Files::FILE_SIZE_LIMIT) {
                            $messages = new Messages();
                            $messages->appendMessage(new Message('Some of your files are too big', 'files'));
                            throw new ValidationException($messages);
                        }
                    }

                    $fileType = $file->getType();
                    $fileName = $file->getName();
                    $file->moveTo(
                        APP_PATH.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$fileName
                    );
                    $dbFile = new Files();
                    $dbFile->setFile($fileName)->setType($fileType)->save();
                    $postFile = new PostFiles();
                    $postFile->setPostId($post->getId())->setFileId(
                        $dbFile->getId()
                    )->save();
                }
            }
            $dbAdapter->commit();
        } catch (Throwable $throwable) {
            $dbAdapter->rollback();
            throw $throwable;
        }

        return $post->refresh();
    }

    private function validateAndPrepareParamters(array $parameters)
    {
        $prepared = [];

        if (array_key_exists('text', $parameters)) {
            $prepared['text'] = $parameters['text'];
        }

        if (array_key_exists('tags', $parameters)) {
            if ( ! empty(trim($parameters['tags']))) {
                $tags = explode(
                    ',',
                    preg_replace('/\s/', '', strtolower($parameters['tags']))
                );

                if (count($tags) > 5) {
                    $messages = new Messages();
                    $messages->appendMessage((new Message('Up to 5 tags allowed.', 'tags')));

                    throw new ValidationException($messages);
                }

                $prepared['tags'] = json_encode($tags);
            }
        }

        if (array_key_exists('group_id', $parameters)) {
            $prepared['group_id'] = intval($parameters['group_id']);
        }

        if (array_key_exists('privacy', $parameters)) {
            $prepared['privacy'] = $parameters['privacy'];
        }

        return $prepared;
    }
}
