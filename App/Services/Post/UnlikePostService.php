<?php

namespace App\Services\Post;

use App\Entities\Posts;
use App\Entities\Users;
use App\Repositories\LikeRepository;
use Phalcon\Di\DiInterface;

class UnlikePostService
{

    private LikeRepository $likeRepository;

    public function __construct(DiInterface $container)
    {
        $this->likeRepository = new LikeRepository($container);
    }

    public function handle(Users $user, Posts $post): void
    {
        $this->likeRepository->delete($post->getId(), $user->getId());
    }
}
