<?php

namespace App\Services\Post;

use App\Entities\Posts;
use App\Entities\Users;
use App\Exceptions\UnauthorisedException;

class DeletePostService
{
    public function handle(Users $user, Posts $post)
    {
        if ($user->getId() !== $post->getAuthor()) {
            throw new UnauthorisedException();
        }

        return $post->delete();
    }
}
