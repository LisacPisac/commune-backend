<?php

namespace App\Services\Post;

use App\Entities\Users;
use App\Repositories\PostRepository;
use App\Services\FilterPostsByPrivacyService;

class GetPostsService
{
    private PostRepository $postRepository;
    private FilterPostsByPrivacyService $filterPostsByPrivacyService;

    public function __construct()
    {
        $this->postRepository = new PostRepository();
        $this->filterPostsByPrivacyService = new FilterPostsByPrivacyService();
    }

    public function handle(Users $authUser, array $parameters = [])
    {
        $prepared = $this->prepareParameters($parameters);

        if (array_key_exists('user_id', $prepared)) {
            $posts = $this->postRepository->getByUser(
                $parameters['user_id'],
                $prepared
            );
        } else {
            $posts = $this->postRepository->get(
                $prepared
            );
        }

        return $this->filterPostsByPrivacyService->handle($posts, $authUser);
    }

    private function prepareParameters(array $parameters): array
    {
        return [
            'limit' => intval($parameters['page_size']),
            'page' => intval($parameters['page']),
        ];
    }
}
