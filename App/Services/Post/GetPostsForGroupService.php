<?php

namespace App\Services\Post;

use App\Entities\Groups;
use App\Entities\Users;
use App\Repositories\PostRepository;
use App\Services\FilterPostsByPrivacyService;

class GetPostsForGroupService
{
    private PostRepository $postRepository;
    private FilterPostsByPrivacyService $filterPostsByPrivacyService;

    public function __construct()
    {
        $this->filterPostsByPrivacyService = new FilterPostsByPrivacyService();
    }

    public function handle(Users $authUser, Groups $group, array $parameters)
    {
        $this->postRepository = new PostRepository();
        $prepared = $this->prepareParameters($parameters);

        $posts = $this->postRepository->getByGroup($group->getId(), $prepared);
        return $this->filterPostsByPrivacyService->handle($posts, $authUser);
    }

    private function prepareParameters(array $parameters): array
    {
        return [
            'limit' => 20,
            'page'  => intval($parameters['page']),
        ];
    }
}
