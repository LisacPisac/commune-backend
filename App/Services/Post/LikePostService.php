<?php

namespace App\Services\Post;

use App\Entities\Likes;
use App\Entities\Posts;
use App\Entities\Users;
use App\Repositories\LikeRepository;
use Phalcon\Di\DiInterface;

class LikePostService
{

    private LikeRepository $likeRepository;

    public function __construct(DiInterface $container)
    {
        $this->likeRepository = new LikeRepository($container);
    }

    public function handle(Users $user, Posts $post): Likes
    {
        return $this->likeRepository->create($post->getId(), $user->getId());
    }
}
