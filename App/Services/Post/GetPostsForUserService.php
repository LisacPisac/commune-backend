<?php

namespace App\Services\Post;

use App\Entities\Users;
use App\Repositories\PostRepository;
use App\Services\FilterPostsByPrivacyService;

class GetPostsForUserService
{
    private PostRepository $postRepository;
    private FilterPostsByPrivacyService $filterPostsByPrivacyService;

    public function __construct()
    {
        $this->filterPostsByPrivacyService = new FilterPostsByPrivacyService();
    }

    public function handle(Users $authUser, Users $user, array $parameters)
    {
        $this->postRepository = new PostRepository();
        $prepared = $this->prepareParameters($parameters);

        $posts = $this->postRepository->getByUser($user->getId(), $prepared);

        return $this->filterPostsByPrivacyService->handle($posts, $authUser);
    }

    private function prepareParameters(array $parameters): array
    {
        return [
            'limit' => $parameters['page_size'],
            'page'  => intval($parameters['page']),
        ];
    }
}
