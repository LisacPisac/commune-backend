<?php

namespace App\Services\Search;

use App\Entities\Users;
use App\Models\SearchResult;
use App\Repositories\GroupRepository;
use App\Repositories\PostRepository;
use App\Repositories\UserRepository;
use App\Services\FilterPostsByPrivacyService;
use Phalcon\Mvc\Model\Resultset\Simple;

class GlobalSearchService
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;
    private GroupRepository $groupRepository;
    private FilterPostsByPrivacyService $filterPostsByPrivacyService;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->postRepository = new PostRepository();
        $this->groupRepository = new GroupRepository();
        $this->filterPostsByPrivacyService = new FilterPostsByPrivacyService();
    }

    public function handle(Users $authUser, array $parameters)
    {
        /** @var Simple $users */
        $users = $this->userRepository->search(
            $parameters['search']
        );

        /** @var Simple $groups */
        $groups = $this->groupRepository->search(
            $parameters['search'],
        );

        $posts = $this->postRepository->search(
            $parameters['search']
        );

        return (new SearchResult())->fromArray(
            [
                'users' => $users,
                'groups' => $groups,
                'posts' => $this->filterPostsByPrivacyService->handle($posts, $authUser)
            ]
        );
    }
}