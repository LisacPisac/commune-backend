<?php

namespace App\Services\Search;

use App\Entities\Users;
use App\Models\SearchResult;
use App\Repositories\PostRepository;
use App\Services\FilterPostsByPrivacyService;

class SearchByTagService
{
    private PostRepository $postRepository;
    private FilterPostsByPrivacyService $filterPostsByPrivacyService;

    public function __construct()
    {
        $this->postRepository = new PostRepository();
        $this->filterPostsByPrivacyService = new FilterPostsByPrivacyService();
    }

    public function handle(Users $authUser, array $parameters)
    {
        $posts = $this->postRepository->searchByTag(
            $parameters['tag']
        );

        return (new SearchResult())->fromArray(
            [
                'users' => [],
                'groups' => [],
                'posts' => $this->filterPostsByPrivacyService->handle($posts, $authUser),
            ]
        );
    }
}
