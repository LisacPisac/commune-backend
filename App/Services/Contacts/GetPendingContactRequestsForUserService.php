<?php

namespace App\Services\Contacts;

use App\Entities\Users;
use App\Repositories\ContactRepository;

class GetPendingContactRequestsForUserService
{
    private ContactRepository $contactRepository;

    public function __construct()
    {
        $this->contactRepository = new ContactRepository();
    }

    public function handle(Users $user)
    {
        return $this->contactRepository->getPendingForUser($user->getId());
    }
}