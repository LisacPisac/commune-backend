<?php

namespace App\Services\Contacts;

use App\Entities\Users;
use App\Repositories\ContactRepository;

class CreateContactService
{
    private ContactRepository $contactRepository;

    public function __construct()
    {
        $this->contactRepository = new ContactRepository();
    }

    public function handle(Users $authUser, Users $contactUser)
    {
        $contact = $this->contactRepository->find($authUser->getId(), $contactUser->getId());
        if ( ! empty($contact)) {
            return $this->contactRepository->update($contact, false);
        } else {
            return $this->contactRepository->create(
                [
                    'user_a' => $authUser->getId(),
                    'user_b' => $contactUser->getId(),
                    'pending' => true,
                ]
            );
        }
    }
}
