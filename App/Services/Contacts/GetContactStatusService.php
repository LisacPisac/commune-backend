<?php

namespace App\Services\Contacts;

use App\Enums\ContactStatusEnum;
use App\Repositories\ContactRepository;

class GetContactStatusService
{
    private ContactRepository $contactRepository;

    public function __construct()
    {
        $this->contactRepository = new ContactRepository();
    }

    public function handle(array $parameters): int
    {
        $contact = $this->contactRepository->find(intval($parameters['user_a']), intval($parameters['user_b']));

        if (empty($contact)) {
            return ContactStatusEnum::NOT_CONTACT;
        } else {
            if ($contact->getPending()) {
                return ContactStatusEnum::PENDING;
            } else {
                return ContactStatusEnum::CONTACTS;
            }
        }
    }
}