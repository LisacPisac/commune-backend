<?php

namespace App\Services\Contacts;

use App\Entities\Users;
use App\Repositories\ContactRepository;

class DeleteContactService
{
    private ContactRepository $contactRepository;

    public function __construct()
    {
        $this->contactRepository = new ContactRepository();
    }

    public function handle(Users $authUser, Users $contactUser)
    {
        return $this->contactRepository->delete($authUser->getId(), $contactUser->getId());
    }
}