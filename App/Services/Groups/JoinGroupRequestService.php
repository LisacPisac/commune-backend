<?php

namespace App\Services\Groups;

use App\Entities\Groups;
use App\Entities\Members;
use App\Entities\Users;
use App\Repositories\MemberRepository;
use Phalcon\Di\DiInterface;

class JoinGroupRequestService
{
    private MemberRepository $memberRepository;

    public function __construct(DiInterface $di)
    {
        $this->memberRepository = new MemberRepository($di);
    }

    public function handle(Users $user, Groups $group): Members
    {
        $membership = $this->memberRepository->createPending(
            $user->getId(),
            $group->getId()
        );

        return $membership->refresh();
    }
}
