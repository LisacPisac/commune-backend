<?php

namespace App\Services\Groups;

use App\Entities\Groups;
use App\Entities\Members;
use App\Entities\Users;
use App\Exceptions\NotFoundException;
use App\Repositories\MemberRepository;
use Phalcon\Di\DiInterface;

class GetJoinRequestStatusService
{
    private MemberRepository $memberRepository;

    public function __construct(DiInterface $di)
    {
        $this->memberRepository = new MemberRepository($di);
    }

    public function handle(Users $user, Groups $group): Members
    {
        $member = $this->memberRepository->find(
            $user->getId(),
            $group->getId()
        ) ?? null;

        if (empty($member)) {
            throw new NotFoundException();
        }

        return $member;
    }
}
