<?php

namespace App\Services\Groups;

use App\Entities\Users;
use App\Repositories\GroupRepository;
use App\Repositories\MemberRepository;
use Phalcon\Di\FactoryDefault;

class CreateGroupService
{
    private GroupRepository $groupRepository;
    private MemberRepository $memberRepository;

    public function __construct()
    {
        $this->groupRepository = new GroupRepository(FactoryDefault::getDefault());
        $this->memberRepository = new MemberRepository(FactoryDefault::getDefault());
    }

    public function handle(Users $authUser, array $parameters)
    {
        $group = $this->groupRepository->create($authUser->getId(), $parameters['name']);

        $this->memberRepository->createActive($group->getOwner(), $group->getId());

        return $group;
    }
}
