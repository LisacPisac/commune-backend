<?php

namespace App\Services\Groups;

use App\Entities\Groups;
use App\Entities\Users;
use App\Exceptions\UnauthorisedException;
use App\Repositories\MemberRepository;
use Phalcon\Di\DiInterface;

class GetPendingMembersService
{
    private MemberRepository $memberRepository;

    public function __construct(DiInterface $di)
    {
        $this->memberRepository = new MemberRepository($di);
    }

    public function handle(Users $user, Groups $group)
    {
        if ($group->getOwner() === $user->getId()) {
            return $this->memberRepository->getPending($group->getId());
        } else {
            if ($this->memberRepository->isAdmin(
                $user->getId(),
                $group->getId()
            )) {
                return $this->memberRepository->getPending($group->getId());
            }
        }

        throw new UnauthorisedException();
    }
}
