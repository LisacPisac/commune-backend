<?php

namespace App\Services\Groups;

use App\Repositories\GroupRepository;
use Phalcon\Di\DiInterface;

class GetGroupsService
{
    private GroupRepository $groupRepository;

    public function __construct(DiInterface $di)
    {
        $this->groupRepository = new GroupRepository($di);
    }

    public function handle(array $parameters)
    {
        return $this->groupRepository->get($parameters);
    }
}
