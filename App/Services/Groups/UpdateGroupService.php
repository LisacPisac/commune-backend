<?php

namespace App\Services\Groups;

use App\Entities\Groups;
use App\Repositories\GroupRepository;
use App\Services\FileHandlerService;
use Phalcon\Di\DiInterface;

class UpdateGroupService
{
    private GroupRepository $groupRepository;
    private FileHandlerService $fileHandlerService;

    public function __construct(DiInterface $diInterface)
    {
        $this->groupRepository = new GroupRepository($diInterface);
        $this->fileHandlerService = new FileHandlerService();
    }

    public function handle(Groups $group, array $parameters, array $files = [])
    {
        /** @var Groups $group */
        $group = $this->groupRepository->update($group->getId(), $parameters);

        foreach ($files as $file) {
            if ($file->getKey() === 'avatar') {
                $group->setGroupAvatarFileId(
                    $this->fileHandlerService->handle($file)->getId()
                )->save();
            }

            if ($file->getKey() === 'cover') {
                $group->setGroupCoverFileId(
                    $this->fileHandlerService->handle($file)->getId()
                )->save();
            }
        }

        return $group->refresh();
    }
}
