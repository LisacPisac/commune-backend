<?php

namespace App\Services\Groups;

use App\Entities\Users;
use App\Repositories\GroupRepository;
use Phalcon\Di\FactoryDefault;

class StoreGroupService
{
    private GroupRepository $groupRepository;

    public function __construct()
    {
        $this->groupRepository = new GroupRepository(FactoryDefault::getDefault());
    }

    public function handle(Users $authUser, array $parameters)
    {
        return $this->groupRepository->create($authUser->getId(), $parameters['name']);
    }
}
