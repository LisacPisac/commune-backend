<?php

namespace App\Services\Groups;

use App\Entities\Groups;
use App\Entities\Users;
use App\Exceptions\UnauthorisedException;
use App\Repositories\GroupRepository;

class DeleteGroupService
{
    private GroupRepository $groupRepository;

    public function __construct()
    {
        $this->groupRepository = new GroupRepository();
    }

    public function handle(Users $authUser, Groups $group)
    {
        if ($authUser->getId() !== $group->getOwner()) {
            throw new UnauthorisedException();
        }

        $this->groupRepository->delete($group->getId());
    }
}
