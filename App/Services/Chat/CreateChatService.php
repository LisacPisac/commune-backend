<?php

namespace App\Services\Chat;

use App\Entities\Users;
use App\Repositories\ChatRepository;

class CreateChatService
{
    private ChatRepository $chatRepository;

    public function __construct()
    {
        $this->chatRepository = new ChatRepository();
    }

    public function handle(Users $authUser, array $parameters)
    {
        $contactUserId = intval($parameters['contact_user_id']);

        $chat = $this->chatRepository->find($authUser->getId(), $contactUserId);

        if ( ! empty($chat->getFirst())) {
            return $chat->getFirst();
        } else {
            return $this->chatRepository->create($authUser->getId(), $contactUserId);
        }
    }
}
