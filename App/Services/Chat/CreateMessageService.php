<?php

namespace App\Services\Chat;

use App\Entities\Users;
use App\Exceptions\NotFoundException;
use App\Repositories\ChatRepository;
use App\Repositories\MessageRepository;

class CreateMessageService
{
    private ChatRepository $chatRepository;
    private MessageRepository $messageRepository;

    public function __construct()
    {
        $this->chatRepository = new ChatRepository();
        $this->messageRepository = new MessageRepository();
    }

    public function handle(Users $authUser, array $parameters)
    {
        $preparedParameters = $this->prepareParameters($parameters);
        $chat = $this->chatRepository->findById($preparedParameters['chat_id']);

        if (empty($chat)) {
            throw new NotFoundException();
        }

        return $this->messageRepository->create(
            $authUser->getId(),
            $preparedParameters['chat_id'],
            $preparedParameters['text']
        );
    }

    private function prepareParameters(array $parameters): array
    {
        return [
            'chat_id'         => intval($parameters['chat_id']),
            'text'            => $parameters['text'],
        ];
    }
}
