<?php

namespace App\Services\Chat;

use App\Entities\Users;
use App\Repositories\MessageRepository;

class IndexChatMessagesService
{
    private MessageRepository $messageRepository;

    public function __construct()
    {
        $this->messageRepository = new MessageRepository();
    }

    public function handle(Users $authUser, array $parameters)
    {
        $preparedParameters = $this->prepareParameters($parameters);

        return $this->messageRepository->index(
            $authUser->getId(),
            $preparedParameters['chat_id'],
            $preparedParameters['page']
        );
    }

    private function prepareParameters(array $parameters)
    {
        return [
            'chat_id' => intval($parameters['chat_id']),
            'page'    => intval($parameters['page']),
        ];
    }

}
