<?php

namespace App\Services\Chat;

use App\Entities\Users;
use App\Repositories\ChatRepository;

class IndexChatsService
{
    private ChatRepository $chatRepository;

    public function __construct()
    {
        $this->chatRepository = new ChatRepository();
    }

    public function handle(Users $authUser)
    {
        return $this->chatRepository->index($authUser->getId());
    }
}
