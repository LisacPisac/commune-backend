<?php

namespace App\Services\Auth;

use App\Exceptions\NotFoundException;
use App\Repositories\PasswordResetRepository;
use App\Repositories\UserRepository;
use Phalcon\Di\FactoryDefault;
use Phalcon\Security;

class ResetPasswordService
{
    private PasswordResetRepository $passwordResetRepository;
    private UserRepository $userRepository;

    public function __construct()
    {
        $this->container = FactoryDefault::getDefault();
        $this->userRepository = new UserRepository();
        $this->passwordResetRepository = new PasswordResetRepository();
    }

    public function handle(array $parameters)
    {
        $passwordReset = $this->passwordResetRepository->findByToken($parameters['token']);

        if ( ! empty($passwordReset)) {
            $user = $passwordReset->getUsers();
            $user = $this->userRepository->update($user, $this->prepareParameters($parameters));

            $this->passwordResetRepository->delete($user->getId());

            return $user;
        } else {
            throw new NotFoundException();
        }
    }

    private function prepareParameters(array $parameters): array
    {
        $prepared = [];

        if (array_key_exists('password', $parameters)) {
            $security = new Security();

            $encryptedPassword = $security->hash($parameters['password']);
            $prepared['password'] = $encryptedPassword;
        }

        return $prepared;
    }
}
