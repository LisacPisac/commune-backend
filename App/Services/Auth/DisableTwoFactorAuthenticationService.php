<?php

namespace App\Services\Auth;

use App\Entities\Users;
use App\Repositories\UserRepository;

class DisableTwoFactorAuthenticationService
{
    private UserRepository $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function handle(Users $user)
    {
        return $this->userRepository->update($user, ['tfa_secret' => null]);
    }
}
