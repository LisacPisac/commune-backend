<?php

namespace App\Services\Auth;

use App\Exceptions\NotFoundException;
use App\Repositories\UserRepository;
use App\Repositories\VerificationRepository;

class VerifyUserService
{
    private VerificationRepository $verificationRepository;
    private UserRepository $userRepository;

    public function __construct()
    {
        $this->verificationRepository = new VerificationRepository();
        $this->userRepository = new UserRepository();
    }

    public function handle(array $parameters)
    {
        $verification = $this->verificationRepository->findByToken($parameters['token']);

        if (empty($verification)) {
            throw new NotFoundException();
        }

        $user = $verification->getUsers();

        $user = $this->userRepository->verify($user->getId());

        return $user->getEnabled();
    }
}
