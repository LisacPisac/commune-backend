<?php

namespace App\Services\Auth;

use App\Entities\Users;
use App\Exceptions\ValidationException;
use App\Repositories\UserRepository;
use Phalcon\Di\DiInterface;
use Phalcon\Di\FactoryDefault;
use Phalcon\Messages\Message;
use Phalcon\Messages\Messages;
use RobThree\Auth\TwoFactorAuth;

class ActivateTwoFactorAuthenticationService
{
    private DiInterface $container;
    private UserRepository $userRepository;

    public function __construct()
    {
        $this->container = FactoryDefault::getDefault();
        $this->userRepository = new UserRepository();
    }

    public function handle(Users $user, array $parameters, string $secret)
    {
        $tfa = new TwoFactorAuth();
        $verified = $tfa->verifyCode($secret, $parameters['code']);

        if ($verified) {
            $this->userRepository->update($user, [
                'tfa_secret' => $secret,
            ]);
            return $user;
        } else {
            $messages = new Messages();
            $messages->appendMessage(new Message('Invalid code', 'tfa_code'));
            throw new ValidationException($messages);
        }
    }
}
