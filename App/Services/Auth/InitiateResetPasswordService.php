<?php

namespace App\Services\Auth;

use App\Exceptions\NotFoundException;
use App\Repositories\PasswordResetRepository;
use App\Repositories\UserRepository;
use App\Services\Mail\SendMailService;

class InitiateResetPasswordService
{
    private PasswordResetRepository $passwordResetRepository;
    private UserRepository $userRepository;
    private SendMailService $sendMailService;

    public function __construct()
    {
        $this->passwordResetRepository = new PasswordResetRepository();
        $this->userRepository = new UserRepository();
        $this->sendMailService = new SendMailService();
    }

    public function handle(array $parameters)
    {
        $user = $this->userRepository->findByEmail($parameters['email']);
        if (!empty($user)) {
            $passwordReset = $this->passwordResetRepository->create($user->getId());

            $this->sendMailService->handle(
                'Password reset request',
                [$user->getEmail()],
                'password_reset',
                [
                    'user' => $user->getUsername(),
                    'token' => $passwordReset->getToken()
                ]
            );
        } else {
            throw new NotFoundException();
        }
    }
}