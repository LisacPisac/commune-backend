<?php

namespace App\Services\Auth;

use Phalcon\Di\DiInterface;
use Phalcon\Session\Manager;

class LogoutService
{
    private DiInterface $container;

    public function __construct(DiInterface $container)
    {
        $this->container = $container;
    }

    public function handle()
    {
        /** @var Manager $session */
        $session = $this->container->getShared('session');
        $session->destroy();
    }
}
