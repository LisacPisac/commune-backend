<?php

namespace App\Services\Auth;

use App\Entities\Users;
use App\Enums\GlobalServicesEnum;
use App\Models\TfaSecret;
use Phalcon\Di\DiInterface;
use Phalcon\Di\FactoryDefault;
use Phalcon\Session\ManagerInterface;
use RobThree\Auth\Providers\Qr\BaconQrCodeProvider;
use RobThree\Auth\TwoFactorAuth;

class GenerateTwoFactorSecretService
{
    private DiInterface $container;

    public function __construct()
    {
        $this->container = FactoryDefault::getDefault();
    }

    public function handle(Users $user): TfaSecret
    {
        $tfa = new TwoFactorAuth(
            'commune',
            6,
            30,
            'sha1',
            new BaconQrCodeProvider()
        );
        $secret = $tfa->createSecret();
        $qrCode = $tfa->getQRCodeImageAsDataUri($user->getUsername(), $secret);
        $qrText = $tfa->getQRText($user->getUsername(), $secret);

        /** @var ManagerInterface $session */
        $session = $this->container->get(GlobalServicesEnum::SESSION);
        $session->set('tfa_secret', $secret);

        return (new TfaSecret($secret, $qrCode, $qrText));
    }
}
