<?php

namespace App\Services\Billing;

use App\Entities\Users;
use App\Repositories\OrderRepository;

class CaptureOrderService
{
    private OrderRepository $orderRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
    }

    public function handle(Users $authUser)
    {
        return $this->orderRepository->capture($authUser);
    }
}
