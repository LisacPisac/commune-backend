<?php

namespace App\Services\Billing;

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;

class PayPalClient
{
    /**
     * Returns PayPal HTTP client instance with environment that has access
     * credentials context. Use this instance to invoke PayPal APIs, provided the
     * credentials have access.
     */

    public static function client()
    {
        return new PayPalHttpClient(self::environment());
    }

    /**
     * Set up and return PayPal PHP SDK environment with PayPal access credentials.
     * This sample uses SandboxEnvironment. In production, use LiveEnvironment.
     */

    public static function environment()
    {
        $clientId = getenv("CLIENT_ID")
            ?: "ASOdarQ9xXhrOzPWHG5Ku32b7T8pInLfmG4IvWLBjr1zWkVpSQk0oVPqYm3Zeb9QoUQp2DiksEj6x8Pw";

        $clientSecret = getenv("CLIENT_SECRET")
            ?: "EPjRvQ9Yxp9BG3iG3-S73NqKZeRuEvaVWL_Obs7hIVZsSlcco-Twf6RNwGkFgEK45-j_kImjVbAYmy8p";

        return new SandboxEnvironment($clientId, $clientSecret);
    }
}
