<?php

namespace App\Services\Billing;

use App\Entities\Users;
use App\Models\Billing\PaypalCreateOrderResponse;
use App\Repositories\OrderRepository;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalHttp\HttpException;
use PayPalHttp\IOException;

class CreateOrderService
{
    private OrderRepository $orderRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
    }

    public function handle(Users $authUser)
    {
        $request = new OrdersCreateRequest();

        $request->prefer('return=representation');

        $request->body = $this->buildRequestBody();

        $client = PayPalClient::client();

        try {
            $response = $client->execute($request);
            $paypalCreateOrderResponse = new PaypalCreateOrderResponse($response->result, $response->headers);

            $this->orderRepository->create($authUser, $paypalCreateOrderResponse);

            return $paypalCreateOrderResponse->links[1]->href;
        } catch (HttpException $e) {
            throw $e;
        } catch (IOException $e) {
            throw $e;
        }
    }

    /**
     * Setting up the JSON request body for creating the order with minimum request body. The intent in the
     * request body should be "AUTHORIZE" for authorize intent flow.
     *
     */

    private function buildRequestBody()
    {
        return [
            'intent'              => 'CAPTURE',
            'application_context' =>
                [
                    'return_url' => $_ENV['SUCCESS_URL'],
                    'cancel_url' => $_ENV['CANCEL_URL'],
                ],
            'purchase_units'      =>
                [
                    [
                        "reference_id" => "test_ref_id1",
                        'description'  => 'Commune premium',
                        'amount'       =>
                            [
                                'currency_code' => 'USD',
                                'value'         => '5.00',
                            ],
                    ],
                ],
        ];
    }
}
