<?php

namespace App\Services;

use App\Entities\Files;
use Phalcon\Http\Request\File;
use Phalcon\Security\Random;

class FileHandlerService
{
    public function handle(File $file)
    {
        $random = new Random();

        $fileType = $file->getType();
        $fileName = $random->uuid();

        $file->moveTo(APP_PATH.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$fileName);

        $dbFile = new Files();
        $dbFile->setFile($fileName)->setType($fileType)->save();

        return $dbFile;
    }
}
