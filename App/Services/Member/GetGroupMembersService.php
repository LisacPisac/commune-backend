<?php

namespace App\Services\Member;

use App\Entities\Groups;
use App\Repositories\MemberRepository;
use Phalcon\Di\DiInterface;

class GetGroupMembersService
{
    private MemberRepository $memberRepository;

    public function __construct(DiInterface $di)
    {
        $this->memberRepository = new MemberRepository($di);
    }

    public function handle(Groups $group)
    {
        return $this->memberRepository->getByGroup($group->getId());
    }
}