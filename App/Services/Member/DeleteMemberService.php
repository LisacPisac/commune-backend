<?php

namespace App\Services\Member;

use App\Entities\Users;
use App\Repositories\MemberRepository;
use Phalcon\Di\DiInterface;

class DeleteMemberService
{
    private MemberRepository $memberRepository;

    public function __construct(DiInterface $di)
    {
        $this->memberRepository = new MemberRepository($di);
    }

    public function handle(Users $authUser, array $parameters)
    {
        return $this->memberRepository->delete(
            $parameters['user_id'],
            $parameters['group_id']
        );
    }
}
