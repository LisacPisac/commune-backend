<?php

namespace App\Services\Member;

use App\Entities\Users;
use App\Exceptions\UnauthorisedException;
use App\Repositories\GroupRepository;
use App\Repositories\MemberRepository;
use Phalcon\Di\DiInterface;

class UpdateMemberService
{
    private MemberRepository $memberRepository;
    private GroupRepository $groupRepository;

    public function __construct(DiInterface $di)
    {
        $this->memberRepository = new MemberRepository($di);
        $this->groupRepository = new GroupRepository($di);
    }

    public function handle(Users $authUser, array $parameters)
    {
        $prepared = $this->prepareParameters($parameters);

        $group = $this->groupRepository->find($prepared['group_id']);

        if ($group->getOwner() === $authUser->getId()) {
            return $this->memberRepository->update(
                $prepared['user_id'],
                $prepared['group_id'],
                $this->prepareParameters($prepared)
            );
        } else {
            if ($this->memberRepository->isAdmin(
                $authUser->getId(),
                $group->getId()
            )) {
                return $this->memberRepository->update(
                    $prepared['user_id'],
                    $prepared['group_id'],
                    $this->prepareParameters($prepared)
                );
            }
        }

        throw new UnauthorisedException();
    }

    private function prepareParameters(array $parameters)
    {
        $prepared = [];

        if (array_key_exists('pending', $parameters)) {
            $prepared['pending'] = $parameters['pending'];
        }

        if (array_key_exists('admin', $parameters)) {
            $prepared['admin'] = $parameters['admin'];
        }

        if (array_key_exists('user_id', $parameters)) {
            $prepared['user_id'] = intval($parameters['user_id']);
        }

        if (array_key_exists('group_id', $parameters)) {
            $prepared['group_id'] = intval($parameters['group_id']);
        }

        return $prepared;
    }
}
