<?php

namespace App\Services\Comment;

use App\Entities\Users;
use App\Repositories\CommentRepository;
use Phalcon\Di\DiInterface;

class CreateCommentService
{
    private DiInterface $container;
    private CommentRepository $commentRepository;

    public function __construct(DiInterface $container)
    {
        $this->container = $container;
        $this->commentRepository = new CommentRepository($this->container);
    }

    public function handle(Users $user, array $parameters)
    {
        return $this->commentRepository->create(
            $user->getId(),
            $this->prepareParameters($parameters)
        );
    }

    private function prepareParameters(array $parameters)
    {
        $prepared = [];

        if (array_key_exists('post_id', $parameters)) {
            $prepared['post_id'] = intval($parameters['post_id']);
        }

        if (array_key_exists('text', $parameters)) {
            $prepared['text'] = $parameters['text'];
        }

        if (array_key_exists('reply_comment_id', $parameters)) {
            $prepared['comment_id'] = $parameters['reply_comment_id'];
        }

        return $prepared;
    }
}
