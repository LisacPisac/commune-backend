<?php

namespace App\Services\Comment;

use App\Entities\Comments;
use App\Entities\Users;
use App\Repositories\CommentRepository;
use Phalcon\Di\DiInterface;

class EditCommentService
{
    private DiInterface $container;
    private CommentRepository $commentRepository;

    public function __construct(DiInterface $container)
    {
        $this->container = $container;
        $this->commentRepository = new CommentRepository($this->container);
    }

    public function handle(Users $user, Comments $comment, array $parameters)
    {
        return $this->commentRepository->update(
            $user->getId(),
            $comment,
            $this->prepareParameters($parameters)
        );
    }

    private function prepareParameters(array $parameters)
    {
        $prepared = [];

        if (array_key_exists('post_id', $parameters)) {
            $prepared['post_id'] = intval($parameters['post_id']);
        }

        if (array_key_exists('text', $parameters)) {
            $prepared['text'] = $parameters['text'];
        }

        return $prepared;
    }
}
