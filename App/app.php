<?php

/**
 * Local variables
 *
 * @var Micro $app
 */

use App\Exceptions\NotFoundException;
use App\Exceptions\UnauthorisedException;
use App\Exceptions\ValidationException;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BillingController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\UserController;
use App\Http\Responses\Response;
use Dotenv\Dotenv;
use Monolog\Logger;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\Collection as MicroCollection;
use Phalcon\Mvc\Model\ValidationFailed;

/**
 * Add your routes here
 */

/** TEST */
$app->mount(
    (new MicroCollection())
        ->setHandler(TestController::class)
        ->setLazy(true)
        ->setPrefix('')
        ->get('/', 'test')
);

/** AUTH */
$app->mount(
    (new MicroCollection())
        ->setHandler(AuthController::class)
        ->setLazy(true)
        ->setPrefix('')
        ->get('/generate-tfa-secret', 'generateTfaSecret')
        ->post('/login', 'login')
        ->post('/logout', 'logout')
        ->post('/init-password-reset', 'initiatePasswordReset')
        ->post('/reset-password', 'resetPassword')
        ->post('/activate-tfa', 'activateTfa')
        ->post('/deactivate-tfa', 'deactivateTfa')
        ->post('/verify', 'verify')
);

/** USER */
$app->mount(
    (new MicroCollection())
        ->setHandler(UserController::class)
        ->setLazy(true)
        ->setPrefix('/users')
        ->get('/{id}', 'get')
        ->get('/profile', 'getProfile')
        ->post('/register', 'register')
        ->post('/update/{id}', 'updateProfile')
        ->delete('/delete/{id}', 'delete')
);

/** POSTS */
$app->mount(
    (new MicroCollection())
        ->setHandler(PostController::class)
        ->setLazy(true)
        ->setPrefix('/posts')
        ->get('/{id}', 'get')
        ->get('/index', 'index')
        ->get('/user/{id}', 'getForUser')
        ->get('/group/{id}', 'getForGroup')
        ->post('/create', 'create')
        ->post('/like/{id}', 'like')
        ->put('/{id}', 'update')
        ->delete('/{id}', 'delete')
        ->delete('/unlike/{id}', 'unlike')
);

/** COMMENTS */
$app->mount(
    (new MicroCollection())
        ->setHandler(CommentController::class)
        ->setLazy(true)
        ->setPrefix('/comments')
        ->get('/get', 'get')
        ->post('/create', 'create')
        ->post('/edit/{id}', 'edit')
        ->post('/reply', 'reply')
);

/** GROUPS */
$app->mount(
    (new MicroCollection())
        ->setHandler(GroupController::class)
        ->setLazy(true)
        ->setPrefix('/groups')
        ->get('/index', 'index')
        ->get('/get/{id}', 'get')
        ->get('/pending-requests/{id}', 'getPending')
        ->post('/create', 'store')
        ->post('/update/{id}', 'update')
        ->post('/join/{id}', 'join')
        ->post('/status/{id}', 'status')
        ->delete('/delete/{id}', 'delete')
);

/** MEMBERS */
$app->mount(
    (new MicroCollection())
        ->setHandler(MemberController::class)
        ->setLazy(true)
        ->setPrefix('/members')
        ->get('/group/{groupId}', 'getForGroup')
        ->get('/status/{groupId}', 'getUserStatus')
        ->post('/update', 'update')
        ->delete('/decline', 'decline')
);

/**
 * CONTACTS
 */
$app->mount(
    (new MicroCollection())
        ->setHandler(ContactController::class)
        ->setLazy(true)
        ->setPrefix('/contacts')
        ->get('/user/{id}', 'getForUser')
        ->get('/pending-count/{id}', 'getPendingCountForUser')
        ->get('/pending/{id}', 'getPendingForUser')
        ->post('/create/{id}', 'store')
        ->post('/status', 'status')
        ->delete('/remove/{id}', 'delete')
);

/**
 * SEARCH
 */
$app->mount(
    (new MicroCollection())
        ->setHandler(SearchController::class)
        ->setLazy(true)
        ->setPrefix('/search')
        ->get('/global', 'globalSearch')
        ->get('/user', 'searchByUser')
        ->get('/tag', 'searchByTag')
);

/** CHAT */
$app->mount(
    (new MicroCollection())
        ->setHandler(ChatController::class)
        ->setLazy(true)
        ->setPrefix('/chats')
        ->get('/chats', 'indexChats')
        ->get('/messages', 'indexMessages')
        ->post('/chats', 'createChat')
        ->post('/messages', 'message')
);

/** BILLING */
$app->mount(
    (new MicroCollection())
        ->setHandler(BillingController::class)
        ->setLazy(true)
        ->setPrefix('/billing')
        ->post('/create', 'createOrder')
        ->post('/capture', 'captureOrder')
        ->post('/cancel', 'cancelOrder')
);

/**
 * Register global services and Helpers
 */
$app->di->setShared(
    'env',
    function () {
        return Dotenv::createImmutable(__DIR__."/..");
    }
);

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders()->send();
});

$app->error(
    function (Throwable $exception) use ($app) {
        /** @var Logger $logger */
        $logger = $app->getDI()->getShared('logger');
        $logger->error($exception->getMessage(), [
            'trace' => $exception->getTraceAsString(),
        ]);
        if ($exception instanceof ValidationException) {
            return (new Response())->setValidationErrorPayload($exception->getMessages());
        }
        if ($exception instanceof NotFoundException) {
            return (new Response())
                ->setStatusCode(Response::NOT_FOUND)
                ->setContent(Response::getHttpCodeDescription(Response::NOT_FOUND))
                ->sendHeaders()
                ->send();
        }
        if ($exception instanceof UnauthorisedException) {
            return (new Response())->setUnauthorizedPayload($exception);
        }
        if ($exception instanceof ValidationFailed) {
            return (new Response())->setValidationErrorPayload($exception->getMessages());
        }
        return (new Response())->setErrorPayload($exception)->send();
    }
);
