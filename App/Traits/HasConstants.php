<?php

namespace App\Traits;

use ReflectionClass;

trait HasConstants
{
    public static function getConstants(?string $prefix = null): array
    {
        $class = new ReflectionClass(__CLASS__);
        return $class->getConstants();
    }
}
