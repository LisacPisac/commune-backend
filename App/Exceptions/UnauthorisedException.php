<?php

namespace App\Exceptions;

use Phalcon\Exception;
use Throwable;

class UnauthorisedException extends Exception implements Throwable
{

}
