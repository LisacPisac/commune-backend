<?php

namespace App\Exceptions;

use Exception;
use Phalcon\Messages\Messages;
use Throwable;

class ValidationException extends Exception implements Throwable
{
    public Messages $messages;

    public function __construct(Messages $messages, $code = 422, Throwable $previous = null)
    {
        $this->messages = $messages;
        parent::__construct('Validation error', $code, $previous);
    }

    public function getMessages(): Messages
    {
        return $this->messages;
    }

}
