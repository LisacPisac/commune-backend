<?php

declare(strict_types=1);

use Phalcon\Cli\Console;
use Phalcon\Di\FactoryDefault;
use Phalcon\Exception as PhalconException;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
const APP_PATH = BASE_PATH.'/App';

require_once(BASE_PATH.'/vendor/autoload.php');

try {
    /**
     * The FactoryDefault Dependency Injector automatically registers the services that
     * provide a full stack framework. These default services can be overidden with custom ones.
     */
    $container = new FactoryDefault\Cli();

    /**
     * Autoload namespaces and directories
     */
    include APP_PATH.'/Config/cli_loader.php';

    /**
     * Include Services
     */
    include APP_PATH.'/Config/services.php';

    /**
     * Start the application
     */
    $app = new Console($container);

    /**
     * Register console modules
     */
    $app->registerModules(
        [
            "cli" => [
                "className" => \App\Cli\Module::class,
                "path"      => APP_PATH.'\Cli\Module.php',
            ],
        ]
    );

    /**
     * Setup the arguments to use the 'cli' module
     */
    $arguments = [];

    /**
     * Process the console arguments
     */
    foreach ($argv as $k => $arg) {
        if ($k == 1) {
            $arguments['task'] = $arg;
        } else {
            if ($k == 2) {
                $arguments['action'] = $arg;
            } else {
                if ($k >= 3) {
                    $arguments['params'][] = $arg;
                }
            }
        }
    }

    /**
     * Handle
     */
    $app->handle($arguments);
} catch (PhalconException $e) {
    fwrite(STDERR, $e->getMessage().PHP_EOL);
    exit(1);
} catch (Throwable $throwable) {
    fwrite(STDERR, $throwable->getMessage().PHP_EOL);
    exit(1);
} catch (Exception $exception) {
    fwrite(STDERR, $exception->getMessage().PHP_EOL);
    exit(1);
}
