<?php

namespace App\Http\Validators;

use Phalcon\Validation;
use Phalcon\Validation\AbstractValidator;

/**
 * Custom validator which conditionally checks the presence of a field, only if another specified field is present as well
 */
class ConditionalPresenceOf extends AbstractValidator
{

    public function validate(Validation $validation, $field): bool
    {
    }
}
