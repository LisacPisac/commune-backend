<?php

namespace App\Http\Validators\Post;

use App\Enums\PostTypesEnum;
use Phalcon\Validation;
use Phalcon\Validation\Validator\InclusionIn;
use Phalcon\Validation\Validator\PresenceOf;

class CreatePostValidator extends Validation
{
    public function initialize()
    {
        $this->add(
            'name',
            new PresenceOf()
        );

        $this->add(
            'post_type_id',
            new InclusionIn(
                [
                    "message" => 'Post type must be '.implode(
                            ',',
                            PostTypesEnum::ALL_TYPES
                        ),
                    "domain"  => PostTypesEnum::ALL_TYPES,
                ]
            )
        );
    }
}
