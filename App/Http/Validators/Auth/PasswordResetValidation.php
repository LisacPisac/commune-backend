<?php

namespace App\Http\Validators\Auth;

use App\Repositories\PasswordResetRepository;
use Phalcon\Validation;

class PasswordResetValidation extends Validation
{
    private PasswordResetRepository $passwordResetRepository;

    public function __construct()
    {
        parent::__construct();
        $this->passwordResetRepository = new PasswordResetRepository();
    }

    public function initialize()
    {
        $this->add(
            'token',
            new Validation\Validator\PresenceOf(
                [
                    'message' => 'The token is required',
                ]
            )
        );

        $this->add(
            'token',
            new Validation\Validator\Callback(
                [
                    'callback' => function (array $data) {
                        return ! empty($this->passwordResetRepository->findByToken($data['token']));
                    },
                    'message'  => 'Token not found',
                ]
            )
        );
    }
}
