<?php

namespace App\Http\Validators\User;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

class UserUpdateValidator extends Validation
{
    public function initialize()
    {
        $this->add(
            'username',
            new PresenceOf(['message' => ':field is required'])
        );
    }
}
