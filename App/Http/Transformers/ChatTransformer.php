<?php

namespace App\Http\Transformers;

use App\Entities\Chats;
use App\Http\Resources\UserResource;
use App\Repositories\UserRepository;
use League\Fractal\TransformerAbstract;

class ChatTransformer extends TransformerAbstract
{
    private UserRepository $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function transform(Chats $chat)
    {
        $userIds = json_decode($chat->getUsers());

        $users = [];

        foreach ($userIds as $id) {
            if ( ! empty($this->userRepository->findById($id))) {
                $users[] = $this->userRepository->findById($id);
            }
        }

        return [
            'id'         => $chat->getId(),
            'name'       => $chat->getName(),
            'users'      => (new UserResource())->collectForResource($users),
            'created_at' => $chat->getCreatedAt(),
            'updated_at' => $chat->getUpdatedAt(),
        ];
    }
}
