<?php

namespace App\Http\Transformers;

use App\Entities\Members;
use App\Http\Resources\GroupResource;
use App\Http\Resources\UserResource;
use League\Fractal\TransformerAbstract;

class MemberTransformer extends TransformerAbstract
{
    public function transform(Members $member): array
    {
        return [
            'group'   => (new GroupResource())->makeForResource(
                $member->getGroups()
            ),
            'user'    => (new UserResource())->makeForResource(
                $member->getUsers()
            ),
            'admin'   => boolval($member->getAdmin()),
            'pending' => boolval($member->getPending()),
        ];
    }
}
