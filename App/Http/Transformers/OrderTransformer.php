<?php

namespace App\Http\Transformers;

use App\Entities\Orders;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    public function transform(Orders $order)
    {
        return $order->toArray();
    }
}
