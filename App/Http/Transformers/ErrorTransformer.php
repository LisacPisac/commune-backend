<?php

namespace App\Http\Transformers;

use League\Fractal\TransformerAbstract;
use Throwable;

class ErrorTransformer extends TransformerAbstract
{
    public function transform(Throwable $exception)
    {
        return [
            'error' => [
                'code'    => $exception->getCode(),
                'status'  => 'error',
                'message' => $exception->getMessage(),
                'trace'   => $exception->getTrace(),
            ],
        ];
    }
}
