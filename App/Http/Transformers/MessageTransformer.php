<?php

namespace App\Http\Transformers;

use App\Entities\Messages;
use App\Http\Resources\UserResource;
use League\Fractal\TransformerAbstract;

class MessageTransformer extends TransformerAbstract
{
    public function transform(Messages $message)
    {
        return [
            'id'         => $message->getId(),
            'text'       => $message->getText(),
            'user'       => (new UserResource())->makeForResource($message->getUsers()),
            'read'       => $message->getRead() ? true : false,
            'created_at' => $message->getCreatedAt(),
        ];
    }
}
