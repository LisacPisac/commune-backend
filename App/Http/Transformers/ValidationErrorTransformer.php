<?php

namespace App\Http\Transformers;

use League\Fractal\TransformerAbstract;
use Phalcon\Messages\Messages;

class ValidationErrorTransformer extends TransformerAbstract
{
    public function transform(Messages $messages)
    {
        return [
            'errors' => $messages->jsonSerialize(),
        ];
    }
}
