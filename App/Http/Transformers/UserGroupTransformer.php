<?php

namespace App\Http\Transformers;

use App\Entities\Groups;
use League\Fractal\TransformerAbstract;

class UserGroupTransformer extends TransformerAbstract
{
    public function transform(Groups $group)
    {
        $avatarFile = $group->getAvatarFile();
        $coverFile = $group->getCoverFile();

        return [
            'id'          => $group->getId(),
            'name'        => $group->getName(),
            'owner'       => $group->getOwner(),
            'avatar_file' => ! empty($avatarFile) ? $avatarFile->toArray()
                : null,
            'cover_file'  => ! empty($coverFile) ? $coverFile->toArray() : null,
            'created_at'  => $group->getCreatedAt(),
            'updated_at'  => $group->getUpdatedAt(),
        ];
    }
}
