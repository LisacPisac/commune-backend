<?php

namespace App\Http\Transformers;

use App\Models\TfaSecret;
use League\Fractal\TransformerAbstract;

class TfaSecretTransformer extends TransformerAbstract
{
    public function transform(TfaSecret $secret)
    {
        return $secret->toArray();
    }
}
