<?php

namespace App\Http\Transformers;

use App\Entities\Members;
use App\Entities\Users;
use App\Http\Resources\UserGroupResource;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(Users $user): array
    {
        $avatarFile = $user->getFiles();

        $avatarInfo = [
            'avatar'      => null,
            'avatar_type' => null,
        ];
        if ( ! empty($avatarFile)) {
            $avatarInfo = [
                'avatar'      => $avatarFile->getFile(),
                'avatar_type' => $avatarFile->getType(),
            ];
        }

        $groups = [];
        $membership = $user->getMembers();
        foreach ($membership as $member) {
            /** @var Members $member */
            $groups[] = $member->getGroups();
        }

        return
            array_merge(
                [
                    'id'          => $user->getId(),
                    'email'       => $user->getEmail(),
                    'subtitle'    => $user->getSubtitle(),
                    'summary'     => $user->getSummary(),
                    'username'    => $user->getUsername(),
                    'enabled'     => $user->getEnabled(),
                    'groups'      => (new UserGroupResource())->collectForResource(
                        $groups
                    ),
                    'tfa_enabled' => ! empty($user->getTfaSecret()),
                    'is_premium'  => $user->isPremium(),
                    'is_deleted'  => $user->getIsDeleted(),
                ],
                $avatarInfo
            );
    }
}
