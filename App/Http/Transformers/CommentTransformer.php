<?php

namespace App\Http\Transformers;

use App\Entities\Comments;
use App\Http\Resources\CommentResource;
use App\Http\Resources\UserResource;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    public function transform(Comments $comment)
    {
        return [
            "id"         => $comment->getId(),
            "post_id"    => $comment->getPostId(),
            "comment_id" => $comment->getCommentId(),
            "text"       => $comment->getText(),
            "author" => ! empty($comment->getUsers()) ? (new UserResource())->makeForResource(
                $comment->getUsers()
            ) : null,
            "created_at" => $comment->getCreatedAt(),
            "updated_at" => $comment->getUpdatedAt(),
            "is_deleted" => $comment->getIsDeleted(),
            'replies'    => (new CommentResource())->collectForResource(
                $comment->getReplies()
            ),
        ];
    }
}
