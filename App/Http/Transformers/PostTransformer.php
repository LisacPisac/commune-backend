<?php

namespace App\Http\Transformers;

use App\Entities\Posts;
use App\Http\Resources\CommentResource;
use App\Http\Resources\GroupResource;
use App\Http\Resources\LikeResource;
use App\Http\Resources\UserResource;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
    public function transform(Posts $post): array
    {
        $files = [];
        foreach ($post->getPostFiles() as $postFile) {
            $files[] = $postFile->getFiles()->toArray();
        }

        $comments = $post->getComments();
        $group = ! empty($post->getGroups()) ? (new GroupResource)->makeForResource($post->getGroups()) : null;

        return [
            'id'             => $post->getId(),
            'text'           => $post->getText(),
            'tags'           => json_decode($post->getTags()),
            'files'          => $files,
            'author'         => (new UserResource())->makeForResource(
                $post->getUsers()
            ),
            'created_at'     => $post->getCreatedAt(),
            'comments'       => (new CommentResource())->collectForResource(
                $comments
            ),
            'group'          => $group,
            'comments_count' => $post->countCommentsWithReplies(),
            'likes_count'    => $post->countLikes(),
            'likes'          => (new LikeResource)->collectForResource($post->getLikes()),
        ];
    }
}
