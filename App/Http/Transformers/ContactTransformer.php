<?php

namespace App\Http\Transformers;

use App\Entities\Contacts;
use App\Http\Resources\UserResource;
use League\Fractal\TransformerAbstract;

class ContactTransformer extends TransformerAbstract
{
    public function transform(Contacts $contact)
    {
        return [
            'user_a' => (new UserResource())->makeForResource($contact->getUserAEntity()),
            'user_b' => (new UserResource())->makeForResource($contact->getUserBEntity()),
            'pending' => $contact->getPending(),
        ];
    }
}