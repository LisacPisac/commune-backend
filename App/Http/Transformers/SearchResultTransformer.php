<?php

namespace App\Http\Transformers;

use App\Http\Resources\GroupResource;
use App\Http\Resources\PostResource;
use App\Http\Resources\UserResource;
use App\Models\SearchResult;
use League\Fractal\TransformerAbstract;

class SearchResultTransformer extends TransformerAbstract
{
    public function transform(SearchResult $searchResult)
    {
        return [
            'users' => (new UserResource())->collectForResource($searchResult->users),
            'groups' => (new GroupResource())->collectForResource($searchResult->groups),
            'posts' => (new PostResource())->collectForResource($searchResult->posts)
        ];
    }
}