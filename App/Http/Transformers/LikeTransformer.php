<?php

namespace App\Http\Transformers;

use App\Entities\Likes;
use League\Fractal\TransformerAbstract;

class LikeTransformer extends TransformerAbstract
{
    public function transform(Likes $like): array
    {
        return [
            "user_id"    => $like->getUserId() ?? null,
            "post_id"    => $like->getPostId(),
            "created_at" => $like->getCreatedAt(),
            "updated_at" => $like->getUpdatedAt(),
        ];
    }
}
