<?php

namespace App\Http\Controllers;

use App\Http\Resources\ChatResource;
use App\Http\Resources\MessageResource;
use App\Services\Chat\CreateChatService;
use App\Services\Chat\CreateMessageService;
use App\Services\Chat\IndexChatMessagesService;
use App\Services\Chat\IndexChatsService;

class ChatController extends BaseController
{
    public function indexChats()
    {
        if ($this->request->isGet()) {
            $user = $this->session->get('user');
            $service = new IndexChatsService();

            return $this->success(
                (new ChatResource())->collect($service->handle($user))
            );
        }

        return $this->badRequest();
    }

    public function indexMessages()
    {
        if ($this->request->isGet()) {
            $user = $this->session->get('user');
            $service = new IndexChatMessagesService();

            return $this->success(
                (new MessageResource())->collect($service->handle($user, $this->request->getQuery()))
            );
        }

        return $this->badRequest();
    }

    public function createChat()
    {
        if ($this->request->isPost()) {
            $user = $this->session->get('user');
            $service = new CreateChatService();

            return $this->success(
                (new ChatResource())->make($service->handle($user, $this->request->getJsonRawBody(true)))
            );
        }

        return $this->badRequest();
    }

    public function message()
    {
        if ($this->request->isPost()) {
            $user = $this->session->get('user');
            $service = new CreateMessageService();

            return $this->success(
                (new MessageResource())->make($service->handle($user, $this->request->getJsonRawBody(true)))
            );
        }

        return $this->badRequest();
    }
}
