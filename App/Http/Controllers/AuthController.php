<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources\AnonymousResource;
use App\Http\Resources\TfaSecretResource;
use App\Http\Resources\UserResource;
use App\Http\Responses\Response;
use App\Http\Validators\Auth\PasswordResetValidation;
use App\Services\Auth\ActivateTwoFactorAuthenticationService;
use App\Services\Auth\DisableTwoFactorAuthenticationService;
use App\Services\Auth\GenerateTwoFactorSecretService;
use App\Services\Auth\InitiateResetPasswordService;
use App\Services\Auth\ResetPasswordService;
use App\Services\Auth\VerifyUserService;
use App\Services\LoginService;

class AuthController extends BaseController
{
    public function login()
    {
        if ($this->request->isPost()) {
            $service = new LoginService();

            $service->handle($this->request->getJsonRawBody(true));
            $this->session->set('user', $service->getUser());

            if ( ! empty($service->getUser())) {
                return $this->success((new UserResource())->make($service->getUser()));
            }
            return $this->notFound();
        }
        return $this->badRequest();
    }

    public function logout()
    {
        if ($this->request->isPost()) {
            $this->session->remove('user');
            $this->session->destroy();

            return (new Response())->setStatusCode(Response::NO_CONTENT)->send();
        }

        return $this->badRequest();
    }

    public function initiatePasswordReset()
    {
        if ($this->request->isPost()) {
            $service = new InitiateResetPasswordService();

            return (new Response())
                ->setSuccessPayload(
                    (new AnonymousResource())->make(
                        [$service->handle($this->request->getJsonRawBody(true))]
                    )
                );
        }

        return $this->badRequest();
    }

    public function resetPassword()
    {
        if ($this->request->isPost()) {
            $validator = new PasswordResetValidation();

            $validator->validate($this->request->getJsonRawBody(true));

            $service = new ResetPasswordService();

            return (new Response())
                ->setSuccessPayload(
                    (new UserResource())->make(
                        $service->handle($this->request->getJsonRawBody(true))
                    )
                );
        }

        return $this->badRequest();
    }

    public function generateTfaSecret()
    {
        if ($this->request->isGet()) {
            $user = $this->session->get('user');
            $service = new GenerateTwoFactorSecretService();

            return $this->success(
                (new TfaSecretResource())->make($service->handle($user))
            );
        }

        return $this->badRequest();
    }

    public function activateTfa()
    {
        if ($this->request->isPost()) {
            $user = $this->session->get('user');
            $service = new ActivateTwoFactorAuthenticationService();

            return $this->success(
                (new UserResource())->make(
                    $service->handle($user, $this->request->getJsonRawBody(true), $this->session->get('tfa_secret'))
                )
            );
        }

        return $this->badRequest();
    }

    public function deactivateTfa()
    {
        if ($this->request->isPost()) {
            $user = $this->session->get('user');
            $service = new DisableTwoFactorAuthenticationService();

            return $this->success(
                (new UserResource())->make($service->handle($user))
            );
        }

        return $this->badRequest();
    }

    public function verify()
    {
        if ($this->request->isPost()) {
            $service = new VerifyUserService();

            return $this->success(
                (new AnonymousResource())->make(
                    [$service->handle($this->request->getJsonRawBody(true))]
                )
            );
        }

        return $this->badRequest();
    }
}
