<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Entities\Groups;
use App\Http\Resources\AnonymousResource;
use App\Http\Resources\GroupResource;
use App\Http\Resources\MemberResource;
use App\Http\Responses\Response;
use App\Services\Groups\CreateGroupService;
use App\Services\Groups\DeleteGroupService;
use App\Services\Groups\GetGroupsService;
use App\Services\Groups\GetPendingMembersService;
use App\Services\Groups\JoinGroupRequestService;
use App\Services\Groups\UpdateGroupService;
use Exception;
use Throwable;

class GroupController extends BaseController
{
    public function index()
    {
        if ($this->request->isGet()) {
            $service = new GetGroupsService($this->di);

            return (new Response())
                ->setSuccessPayload(
                    (new GroupResource())->collect(
                        $service->handle($this->request->getQuery())
                    )
                )
                ->send();
        }

        return $this->badRequest();
    }

    public function get(Groups $group)
    {
        if ($this->request->isGet()) {
            return (new Response())
                ->setSuccessPayload(
                    (new GroupResource())->make(
                        $group
                    )
                )
                ->send();
        }

        return $this->badRequest();
    }

    public function store()
    {
        if ($this->request->isPost()) {
            $this->db->begin();
            try {
                $authUser = $this->session->get('user');
                $service = new CreateGroupService();

                $this->db->commit();

                return (new Response())
                    ->setSuccessPayload(
                        (new GroupResource())->make(
                            $service->handle($authUser, $this->request->getJsonRawBody(true))
                        )
                    );
            } catch (Throwable $throwable) {
                $this->db->rollback();
            }
        }

        return $this->badRequest();
    }

    public function update(Groups $group)
    {
        if ($this->request->isPost()) {
            $this->db->begin();
            try {
                $service = new UpdateGroupService($this->di);
                $result = $service->handle(
                    $group,
                    $this->request->getPost(),
                    $this->request->getUploadedFiles()
                );

                $this->db->commit();

                return (new Response())
                    ->setSuccessPayload((new GroupResource())->make($result))
                    ->send();
            } catch (Exception $exception) {
                $this->db->rollback();
            }
        }

        return $this->badRequest();
    }

    public function delete(Groups $group)
    {
        if ($this->request->isDelete()) {
            $user = $this->session->get('user');
            $service = new DeleteGroupService();

            return $this->success(
                (new AnonymousResource)->make(
                    [$service->handle($user, $group)]
                )
            );
        }

        return $this->badRequest();
    }

    public function join(Groups $group)
    {
        if ($this->request->isPost()) {
            try {
                $this->db->begin();
                $authUser = $this->session->get('user');
                $service = new JoinGroupRequestService($this->di);

                $response = (new Response())
                    ->setSuccessPayload(
                        (new MemberResource())->make(
                            $service->handle($authUser, $group)
                        )
                    );

                $this->db->commit();

                return $response->send();
            } catch (Throwable $throwable) {
                $this->db->rollback();
                throw $throwable;
            }
        }

        return $this->badRequest();
    }

    public function getPending(Groups $group)
    {
        if ($this->request->isGet()) {
            $user = $this->session->get('user');
            $service = new GetPendingMembersService($this->di);

            return (new MemberResource())->collect(
                $service->handle($user, $group)
            );
        }

        return $this->badRequest();
    }
}
