<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Responses\Response;
use Phalcon\Http\ResponseInterface;
use Phalcon\Messages\Messages;
use Phalcon\Mvc\Controller;
use Phalcon\Validation;

class BaseController extends Controller
{
    protected function validateRequest(Validation $validator)
    {
        $validationResult = null;

        if ($this->request->isGet()) {
            $validationResult = $validator->validate($this->request->get());
        }

        if ($this->request->isPost()) {
            $validationResult = $validator->validate($this->request->getJsonRawBody(true));
        }

        if ($this->request->isPut()) {
            $validationResult = $validator->validate($this->request->getJsonRawBody(true));
        }

        if ( ! empty($validationResult)) {
            /** @var Messages $validationResult */
            if ($validationResult) {
                return (new Response())->setValidationErrorPayload(
                    $validationResult
                );
            }
        }
    }

    protected function success(string $payload): ResponseInterface
    {
        return (new Response())
            ->setSuccessPayload($payload)
            ->send();
    }

    protected function notFound(): ResponseInterface
    {
        return (new Response())
            ->setStatusCode(Response::NOT_FOUND)
            ->setContent(Response::getHttpCodeDescription(Response::NOT_FOUND))
            ->sendHeaders()
            ->send();
    }

    protected function badRequest(): ResponseInterface
    {
        return (new Response())
            ->setStatusCode(Response::BAD_REQUEST)
            ->setContent(
                Response::getHttpCodeDescription(Response::BAD_REQUEST)
            )
            ->sendHeaders()
            ->send();
    }

    protected function checkSession()
    {
        if (empty($this->session->get('user'))) {
            return (new Response())
                ->setStatusCode(Response::FORBIDDEN)
                ->setContent(
                    Response::getHttpCodeDescription(Response::FORBIDDEN)
                )
                ->sendHeaders()
                ->send();
        }
    }
}
