<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Entities\Groups;
use App\Entities\Posts;
use App\Entities\Users;
use App\Http\Resources\AnonymousResource;
use App\Http\Resources\LikeResource;
use App\Http\Resources\PostResource;
use App\Http\Responses\Response;
use App\Services\Post\CreatePostService;
use App\Services\Post\DeletePostService;
use App\Services\Post\GetPostsForGroupService;
use App\Services\Post\GetPostsForUserService;
use App\Services\Post\GetPostsService;
use App\Services\Post\LikePostService;
use App\Services\Post\UnlikePostService;

class PostController extends BaseController
{
    public function index()
    {
        if ($this->request->isGet()) {
            $user = $this->session->get('user');
            $service = new GetPostsService();

            return (new Response())
                ->setSuccessPayload(
                    (new PostResource())->collect(
                        $service->handle($user, $this->request->getQuery())
                    )
                )
                ->send();
        }

        return $this->badRequest();
    }

    public function getForUser(Users $user)
    {
        if ($this->request->isGet()) {
            $authUser = $this->session->get('user');
            $service = new GetPostsForUserService();

            return (new Response())
                ->setSuccessPayload(
                    (new PostResource())->collect(
                        $service->handle(
                            $authUser,
                            $user,
                            $this->request->get(),
                        )
                    )
                )
                ->send();
        }

        return $this->badRequest();
    }

    public function getForGroup(Groups $group)
    {
        if ($this->request->isGet()) {
            $user = $this->session->get('user');
            $service = new GetPostsForGroupService();

            return (new Response())
                ->setSuccessPayload(
                    (new PostResource())->collect(
                        $service->handle(
                            $user,
                            $group,
                            $this->request->get(),
                        )
                    )
                )
                ->send();
        }

        return $this->badRequest();
    }

    public function create()
    {
        if ($this->request->isPost()) {
            $authUser = $this->session->get('user');
            $service = new CreatePostService();

            return (new Response())
                ->setSuccessPayload(
                    (new PostResource())->make(
                        $service->handle(
                            $authUser,
                            $this->request->getPost(),
                            $this->request->getUploadedFiles()
                        )
                    )
                )
                ->send();
        }

        return $this->badRequest();
    }

    public function delete(Posts $post)
    {
        if ($this->request->isDelete()) {
            $authUser = $this->session->get('user');
            $service = new DeletePostService();

            return (new Response())
                ->setSuccessPayload(
                    json_encode($service->handle($authUser, $post))
                )
                ->send();
        }

        return $this->badRequest();
    }

    public function like(Posts $post)
    {
        if ($this->request->isPost()) {
            $authUser = $this->session->get('user');
            $service = new LikePostService($this->di);

            return (new Response())
                ->setSuccessPayload(
                    (new LikeResource())->make(
                        $service->handle($authUser, $post)
                    )
                )
                ->send();
        }

        return $this->badRequest();
    }

    public function unlike(Posts $post)
    {
        if ($this->request->isDelete()) {
            $authUser = $this->session->get('user');
            $service = new UnlikePostService($this->di);

            return (new Response())
                ->setSuccessPayload(
                    (new AnonymousResource())->make(
                        [$service->handle($authUser, $post)]
                    )
                )
                ->send();
        }

        return $this->badRequest();
    }
}
