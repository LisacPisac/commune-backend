<?php

namespace App\Http\Controllers;

use App\Http\Resources\SearchResultResource;
use App\Services\Search\GlobalSearchService;
use App\Services\Search\SearchByTagService;
use App\Services\Search\SearchByUserService;

class SearchController extends BaseController
{
    public function globalSearch()
    {
        if ($this->request->get()) {
            $user = $this->session->get('user');
            $service = new GlobalSearchService();

            return $this->success(
                (new SearchResultResource())->make($service->handle($user, $this->request->get()))
            );
        }

        return $this->badRequest();
    }

    public function searchByUser()
    {
        if ($this->request->get()) {
            $user = $this->session->get('user');
            $service = new SearchByUserService();

            return $this->success(
                (new SearchResultResource())->make($service->handle($user, $this->request->get()))
            );
        }

        return $this->badRequest();
    }

    public function searchByTag()
    {
        if ($this->request->get()) {
            $user = $this->session->get('user');
            $service = new SearchByTagService();

            return $this->success(
                (new SearchResultResource())->make($service->handle($user, $this->request->get()))
            );
        }

        return $this->badRequest();
    }
}
