<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Services\Billing\CancelOrderService;
use App\Services\Billing\CaptureOrderService;
use App\Services\Billing\CreateOrderService;

class BillingController extends BaseController
{
    public function createOrder()
    {
        if ($this->request->isPost()) {
            $user = $this->session->get('user');
            $service = new CreateOrderService();

            return $this->success(json_encode($service->handle($user)));
        }

        return $this->badRequest();
    }

    public function captureOrder()
    {
        if ($this->request->isPost()) {
            $user = $this->session->get('user');
            $service = new CaptureOrderService();

            return $this->success((new OrderResource())->make($service->handle($user)));
        }

        return $this->badRequest();
    }

    public function cancelOrder()
    {
        if ($this->request->isPost()) {
            $user = $this->session->get('user');
            $service = new CancelOrderService();

            return $this->success($service->handle($user));
        }

        return $this->badRequest();
    }
}
