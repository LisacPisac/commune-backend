<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Entities\Users;
use App\Http\Resources\UserResource;
use App\Http\Responses\Response;
use App\Services\User\DeleteUserService;
use App\Services\User\GetProfileService;
use App\Services\User\RegisterUserService;
use App\Services\User\UpdateUserService;
use Phalcon\Http\ResponseInterface;

class UserController extends BaseController
{
    public static function getModelName(): string
    {
        return Users::class;
    }

    public function get(Users $user): ResponseInterface
    {
        if ($this->request->isGet()) {
            return (new Response())->setSuccessPayload(
                (new UserResource())->make($user)
            );
        }

        return $this->badRequest();
    }

    public function getProfile(): ResponseInterface
    {
        if ($this->request->isGet()) {
            // if (empty($this->session->get('user'))) {
            //     $service = new LogoutService($this->di);
            //     $service->handle();
            //     return $this->notFound();
            // }

            $service = new GetProfileService();

            return (new Response())->setSuccessPayload(
                (new UserResource())->make($service->handle($this->session->get('user')))
            );
        }

        return $this->badRequest();
    }

    public function register(): ResponseInterface
    {
        if ($this->request->isPost()) {
            $service = new RegisterUserService($this->di);

            return (new Response())
                ->setSuccessPayload(
                    (new UserResource())->make(
                        $service->handle(
                            $this->request->getJsonRawBody(true)
                        )
                    )
                )
                ->send();
        }

        return $this->badRequest();
    }

    public function updateProfile(Users $user): ResponseInterface
    {
        if ($this->request->isPost()) {
            $service = new UpdateUserService();

            $user = $service->handle(
                $user,
                $this->request->getPost(),
                $this->request->getUploadedFiles()
            );
            $this->session->set('user', $user);

            return (new Response())
                ->setSuccessPayload(
                    (new UserResource())->make($user)
                )
                ->send();
        }

        return $this->badRequest();
    }

    public function delete(Users $user): ResponseInterface
    {
        if ($this->request->isDelete()) {
            $service = new DeleteUserService();

            return $this->success(json_encode([$service->handle($user)]));
        }

        return $this->badRequest();
    }
}

