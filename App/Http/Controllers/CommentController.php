<?php

namespace App\Http\Controllers;

use App\Entities\Comments;
use App\Http\Resources\CommentResource;
use App\Http\Responses\Response;
use App\Services\Comment\CreateCommentService;
use App\Services\Comment\EditCommentService;

class CommentController extends BaseController
{
    public function get()
    {
    }

    public function create()
    {
        if ($this->request->isPost()) {
            $service = new CreateCommentService($this->di);

            $user = $this->session->get('user');

            return (new Response())
                ->setSuccessPayload(
                    (new CommentResource())->make(
                        $service->handle(
                            $user,
                            $this->request->getJsonRawBody(true)
                        )
                    )
                )
                ->send();
        } else {
            return $this->badRequest();
        }
    }

    public function edit(Comments $comment)
    {
        if ($this->request->isPost()) {
            $authUser = $this->session->get('user');
            $service = new EditCommentService($this->di);

            return (new Response())
                ->setSuccessPayload(
                    (new CommentResource())->make(
                        $service->handle(
                            $authUser,
                            $comment,
                            $this->request->getJsonRawBody(true)
                        )
                    )
                )
                ->send();
        } else {
            return $this->badRequest();
        }
    }

    public function reply()
    {
        if ($this->request->isPost()) {
            $authUser = $this->session->get('user');
            $service = new CreateCommentService($this->di);

            return (new Response())
                ->setSuccessPayload(
                    (new CommentResource())->make(
                        $service->handle(
                            $authUser,
                            $this->request->getJsonRawBody(true)
                        )
                    )
                )
                ->send();
        } else {
            return $this->badRequest();
        }
    }
}
