<?php

namespace App\Http\Controllers;

use App\Entities\Users;
use App\Http\Resources\AnonymousResource;
use App\Http\Resources\ContactResource;
use App\Http\Resources\UserResource;
use App\Http\Responses\Response;
use App\Services\Contacts\CreateContactService;
use App\Services\Contacts\DeleteContactService;
use App\Services\Contacts\GetContactsByUserService;
use App\Services\Contacts\GetContactStatusService;
use App\Services\Contacts\GetPendingContactRequestCountForUserService;
use App\Services\Contacts\GetPendingContactRequestsForUserService;

class ContactController extends BaseController
{
    public function get()
    {
        if ($this->request->isGet()) {
        }

        return $this->badRequest();
    }

    public function index()
    {
        if ($this->request->isGet()) {
        }

        return $this->badRequest();
    }

    public function getForUser(Users $user)
    {
        if ($this->request->isGet()) {
            $service = new GetContactsByUserService();

            return (new UserResource())->collect(
                $service->handle($user)
            );
        }

        return $this->badRequest();
    }

    public function getPendingForUser(Users $user)
    {
        if ($this->request->isGet()) {
            $service = new GetPendingContactRequestsForUserService();

            return $this->success((new ContactResource())->collect($service->handle($user)));
        }

        return $this->badRequest();
    }

    public function getPendingCountForUser(Users $user)
    {
        if ($this->request->isGet()) {
            $service = new GetPendingContactRequestCountForUserService();

            return $this->success((new AnonymousResource())->make([$service->handle($user)]));
        }

        return $this->badRequest();
    }

    public function store(Users $contactUser)
    {
        if ($this->request->isPost()) {
            $authUser = $this->session->get('user');
            $service = new CreateContactService();

            return (new Response())->setSuccessPayload(
                (new ContactResource())->make(
                    $service->handle($authUser, $contactUser)
                )
            );
        }

        return $this->badRequest();
    }

    public function delete(Users $contactUser)
    {
        if ($this->request->isDelete()) {
            $authUser = $this->session->get('user');
            $service = new DeleteContactService();

            return (new Response())->setSuccessPayload(
                (new AnonymousResource())->make(
                    [$service->handle($authUser, $contactUser)]
                )
            );
        }

        return $this->badRequest();
    }

    public function status()
    {
        if ($this->request->isPost()) {
            $service = new GetContactStatusService();

            return (new Response())->setSuccessPayload(
                (new AnonymousResource())->make(
                    ['status' => $service->handle($this->request->getJsonRawBody(true))]
                )
            );
        }

        return $this->badRequest();
    }
}
