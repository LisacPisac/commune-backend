<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection as FractalCollection;
use Phalcon\Mvc\Controller;

class TestController extends Controller
{
    public function test(): string
    {
        // phpinfo();
        $fractal = new Manager();

        $resource = new FractalCollection(
            [
                ['test' => $_ENV['DEBUG'], 'gd_info' => gd_info()],
            ],
            function (array $data) {
                return [
                    'test' => $data['test'],
                    'gd_info' => $data['gd_info'],
                ];
            }
        );

        return $fractal->createData($resource)->toJson();
    }
}
