<?php

namespace App\Http\Controllers;

use App\Entities\Groups;
use App\Http\Resources\MemberResource;
use App\Http\Responses\Response;
use App\Services\Groups\GetJoinRequestStatusService;
use App\Services\Member\DeleteMemberService;
use App\Services\Member\GetGroupMembersService;
use App\Services\Member\UpdateMemberService;

class MemberController extends BaseController
{
    public function getForGroup(Groups $group)
    {
        if ($this->request->isGet()) {
            $service = new GetGroupMembersService($this->di);

            return (new Response())->setSuccessPayload(
                (new MemberResource())->collect($service->handle($group))
            )->send();
        }

        return $this->badRequest();
    }

    public function getUserStatus(Groups $group)
    {
        if ($this->request->isGet()) {
            $authUser = $this->session->get('user');
            $service = new GetJoinRequestStatusService($this->di);

            $response = (new Response())
                ->setSuccessPayload(
                    (new MemberResource())->make(
                        $service->handle($authUser, $group)
                    )
                );

            return $response->send();
        }

        return $this->badRequest();
    }

    public function update()
    {
        if ($this->request->isPost()) {
            $authUser = $this->session->get('user');
            $service = new UpdateMemberService($this->di);

            return (new MemberResource())->make(
                $service->handle($authUser, $this->request->getJsonRawBody(true))
            );
        }

        return $this->badRequest();
    }

    public function decline()
    {
        if ($this->request->isDelete()) {
            $authUser = $this->session->get('user');
            $service = new DeleteMemberService($this->di);

            return json_encode(
                $service->handle($authUser, $this->request->getQuery())
            );
        }

        return $this->badRequest();
    }
}
