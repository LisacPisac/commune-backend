<?php

namespace App\Http\Responses;

use App\Http\Resources\ErrorResource;
use App\Http\Resources\ValidationErrorResource;
use Phalcon\Http\ResponseInterface;
use Phalcon\Messages\Messages;
use Throwable;

class Response extends \Phalcon\Http\Response
{
    public const OK = 200;
    public const CREATED = 201;
    public const ACCEPTED = 202;
    public const NO_CONTENT = 204;
    public const MOVED_PERMANENTLY = 301;
    public const FOUND = 302;
    public const TEMPORARY_REDIRECT = 307;
    public const PERMANENTLY_REDIRECT = 308;
    public const BAD_REQUEST = 400;
    public const UNAUTHORIZED = 401;
    public const FORBIDDEN = 403;
    public const NOT_FOUND = 404;
    public const UNPROCESSABLE_ENTTIY = 422;
    public const INTERNAL_SERVER_ERROR = 500;
    public const NOT_IMPLEMENTED = 501;
    public const BAD_GATEWAY = 502;

    public const CODES
        = [
            200 => 'OK',
            201 => 'Created',
            204 => 'No Content',
            301 => 'Moved Permanently',
            302 => 'Found',
            307 => 'Temporary Redirect',
            308 => 'Permanent Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            422 => 'Unprocessable Entity',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
        ];

    /**
     * Returns the Http code description or if not found the code itself
     *
     * @param int $code
     *
     * @return int|string
     */
    public static function getHttpCodeDescription(int $code)
    {
        if (true === isset(self::CODES[$code])) {
            return sprintf('%d (%s)', $code, self::CODES[$code]);
        }

        return $code;
    }

    public function send(): ResponseInterface
    {
//        $this
//            ->setHeader('Access-Control-Allow-Origin', '*')
//            ->setHeader("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE,OPTIONS')
//            ->setHeader("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Authorization')
//            ->setHeader("Access-Control-Allow-Credentials", true);
        parent::sendHeaders();
        return parent::send();
    }

    public function setSuccessPayload(string $jsonData): Response
    {
        $this
            ->setStatusCode(self::OK)
            ->setContentType('application/json')
            ->setContent($jsonData);
        return $this;
    }

    public function setErrorPayload(Throwable $exception)
    {
        $this
            ->setStatusCode(self::INTERNAL_SERVER_ERROR)
            ->setContentType('application/json')
            ->setContent((new ErrorResource())->make($exception));
        return $this;
    }

    public function setValidationErrorPayload(Messages $message)
    {
        $this
            ->setStatusCode(self::UNPROCESSABLE_ENTTIY)
            ->setContentType('application/json')
            ->setContent((new ValidationErrorResource())->make($message));
        return $this;
    }

    public function setUnauthorizedPayload(Throwable $exception)
    {
        $this
            ->setStatusCode(self::UNAUTHORIZED)
            ->setContentType('application/json')
            ->setContent(json_encode($exception->getMessage()));
        return $this;
    }
}
