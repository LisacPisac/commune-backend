<?php

namespace App\Http\Resources;

use App\Entities\Members;
use App\Http\Transformers\MemberTransformer;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;

class MemberResource extends AbstractResource
{

    public function make(Members $member): string
    {
        $resource = new FractalItem(
            $member,
            new MemberTransformer()
        );

        $this->activateDataArraySerializer();
        return $this->manager->createData($resource)->toJson();
    }

    public function collect($members): string
    {
        $resource = new FractalCollection(
            $members,
            new MemberTransformer()
        );

        return $this->manager->createData($resource)->toJson();
    }

    public function makeForResource(Members $member): array
    {
        $resource = new FractalItem(
            $member,
            new MemberTransformer()
        );

        $this->activateSubDataSerializer();
        return $this->manager->createData($resource)->toArray();
    }

    public function collectForResource($members): array
    {
        $resource = new FractalCollection(
            $members,
            new MemberTransformer()
        );

        $this->activateSubDataSerializer();
        return $this->manager->createData($resource)->toArray();
    }
}
