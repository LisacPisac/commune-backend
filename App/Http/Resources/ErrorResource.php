<?php

namespace App\Http\Resources;

use App\Http\Transformers\ErrorTransformer;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;
use Phalcon\Collection;
use Throwable;

class ErrorResource extends AbstractResource
{
    public function make(Throwable $exception): string
    {
        $resource = new FractalItem(
            $exception,
            new ErrorTransformer()
        );

        return $this->manager->createData($resource)->toJson();
    }

    public function collect(Collection $errors): string
    {
        $resource = new FractalCollection(
            [
                $errors,
            ],
            new ErrorTransformer()
        );

        return $this->manager->createData($resource)->toJson();
    }

    public function makeForResource(): array
    {
        return [];
    }

    public function collectForResource(): array
    {
        return [];
    }
}
