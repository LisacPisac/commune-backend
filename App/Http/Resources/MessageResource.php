<?php

namespace App\Http\Resources;

use App\Entities\Messages;
use App\Http\Transformers\MessageTransformer;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;

class MessageResource extends AbstractResource
{
    public function make(Messages $message): string
    {
        $resource = new FractalItem(
            $message,
            new MessageTransformer
        );

        $this->activateDataArraySerializer();
        return $this->sendJsonData($resource);
    }

    public function collect($messages): string
    {
        $resource = new FractalCollection(
            $messages,
            new MessageTransformer
        );

        $this->activateDataArraySerializer();
        return $this->sendJsonData($resource);
    }
}
