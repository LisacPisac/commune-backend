<?php

namespace App\Http\Resources;

use App\Http\Resources\Serializers\ArraySerializer;
use App\Http\Resources\Serializers\SubDataSerializer;
use League\Fractal\Manager;
use League\Fractal\Resource\ResourceInterface;
use League\Fractal\Serializer\DataArraySerializer;

abstract class AbstractResource
{
    protected Manager $manager;
    protected DataArraySerializer $dataArraySerializer;
    protected SubDataSerializer $subDataSerializer;
    protected ArraySerializer $arraySerializer;

    public function __construct()
    {
        $this->manager = new Manager();
        $this->dataArraySerializer = new DataArraySerializer();
        $this->subDataSerializer = new SubDataSerializer();
        $this->arraySerializer = new ArraySerializer();
    }

    protected function activateArraySerializer()
    {
        $this->manager->setSerializer($this->arraySerializer);
    }

    protected function activateDataArraySerializer()
    {
        $this->manager->setSerializer($this->dataArraySerializer);
    }

    protected function activateSubDataSerializer()
    {
        $this->manager->setSerializer($this->subDataSerializer);
    }

    protected function sendJsonData(ResourceInterface $resource): string
    {
        return $this->manager->createData($resource)->toJson();
    }

    protected function sendArrayData(ResourceInterface $resource): array
    {
        return $this->manager->createData($resource)->toArray();
    }
}
