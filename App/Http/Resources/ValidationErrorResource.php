<?php

namespace App\Http\Resources;

use App\Http\Transformers\ValidationErrorTransformer;
use League\Fractal\Resource\Item as FractalItem;
use Phalcon\Messages\Messages;

class ValidationErrorResource extends AbstractResource
{
    public function make(Messages $message): string
    {
        $resource = new FractalItem(
            $message,
            new ValidationErrorTransformer()
        );

        return $this->manager->createData($resource)->toJson();
    }
}
