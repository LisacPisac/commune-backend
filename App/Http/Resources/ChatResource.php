<?php

namespace App\Http\Resources;

use App\Entities\Chats;
use App\Http\Transformers\ChatTransformer;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;

class ChatResource extends AbstractResource
{
    public function make(Chats $chat): string
    {
        $resource = new FractalItem(
            $chat,
            new ChatTransformer()
        );

        $this->activateDataArraySerializer();
        return $this->sendJsonData($resource);
    }

    public function collect($chats): string
    {
        $resource = new FractalCollection(
            $chats,
            new ChatTransformer()
        );

        $this->activateDataArraySerializer();
        return $this->sendJsonData($resource);
    }
}
