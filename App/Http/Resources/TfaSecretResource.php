<?php

namespace App\Http\Resources;

use App\Http\Transformers\TfaSecretTransformer;
use App\Models\TfaSecret;
use League\Fractal\Resource\Item as FractalItem;

class TfaSecretResource extends AbstractResource
{
    public function make(TfaSecret $secret): string
    {
        $resource = new FractalItem(
            $secret,
            new TfaSecretTransformer()
        );

        return $this->manager->createData($resource)->toJson();
    }
}
