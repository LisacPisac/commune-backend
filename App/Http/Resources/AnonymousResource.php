<?php

namespace App\Http\Resources;

use League\Fractal\Resource\Item as FractalItem;

class AnonymousResource extends AbstractResource
{
    public function make(array $value)
    {
        $resource = new FractalItem(
            $value,
            function (array $value) {
                return $value;
            }
        );

        return $this->sendJsonData($resource);
    }
}
