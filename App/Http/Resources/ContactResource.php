<?php

namespace App\Http\Resources;

use App\Entities\Contacts;
use App\Http\Transformers\ContactTransformer;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;

class ContactResource extends AbstractResource
{
    public function make(Contacts $contact)
    {
        $resource = new FractalItem(
            $contact,
            new ContactTransformer
        );

        $this->activateDataArraySerializer();
        return $this->sendJsonData($resource);
    }

    public function collect($contacts)
    {
        $resource = new FractalCollection(
            $contacts,
            new ContactTransformer
        );

        $this->activateDataArraySerializer();
        return $this->sendJsonData($resource);
    }
}