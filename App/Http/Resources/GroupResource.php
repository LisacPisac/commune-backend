<?php

namespace App\Http\Resources;

use App\Entities\Groups;
use App\Http\Transformers\GroupTransformer;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;

class GroupResource extends AbstractResource
{

    public function make(Groups $group): string
    {
        $resource = new FractalItem(
            $group,
            new GroupTransformer()
        );

        $this->activateDataArraySerializer();
        return $this->sendJsonData($resource);
    }

    public function makeForResource(Groups $group)
    {
        $resource = new FractalItem(
            $group,
            new GroupTransformer()
        );

        $this->activateSubDataSerializer();
        return $this->sendArrayData($resource)[0];
    }

    public function collect($groups): string
    {
        $resource = new FractalCollection(
            $groups,
            new GroupTransformer()
        );

        return $this->sendJsonData($resource);
    }

    public function collectForResource($groups): array
    {
        $resource = new FractalCollection(
            $groups,
            new GroupTransformer()
        );

        $this->activateSubDataSerializer();
        return $this->sendArrayData($resource);
    }
}
