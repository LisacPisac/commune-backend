<?php

namespace App\Http\Resources;

use App\Entities\Groups;
use App\Http\Transformers\UserGroupTransformer;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;

/**
 * Same as UserResource, except it doesn't fetch owner info, which fetches group info, which fetches owner info .... you get the idea, recursion
 */
class UserGroupResource extends AbstractResource
{
    public function make(Groups $group): string
    {
        $resource = new FractalItem(
            $group,
            new UserGroupTransformer()
        );

        $this->activateDataArraySerializer();
        return $this->sendJsonData($resource);
    }

    public function collect($groups): string
    {
        $resource = new FractalCollection(
            $groups,
            new UserGroupTransformer()
        );

        return $this->sendJsonData($resource);
    }

    public function collectForResource($groups)
    {
        $resource = new FractalCollection(
            $groups,
            new UserGroupTransformer()
        );

        $this->activateSubDataSerializer();
        return $this->sendArrayData($resource);
    }
}
