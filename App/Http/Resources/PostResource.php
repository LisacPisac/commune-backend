<?php

namespace App\Http\Resources;

use App\Entities\Posts;
use App\Http\Transformers\PostTransformer;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;

class PostResource extends AbstractResource
{
    public function make(Posts $posts): string
    {
        $resource = new FractalItem(
            $posts,
            new PostTransformer()
        );

        return $this->manager->createData($resource)->toJson();
    }

    public function collect($posts): string
    {
        $resource = new FractalCollection(
            $posts,
            new PostTransformer()
        );

        return $this->manager->createData($resource)->toJson();
    }

    public function makeForResource(Posts $post): array
    {
        return [];
    }

    public function collectForResource($posts): array
    {
        $resource = new FractalCollection(
            $posts,
            new PostTransformer()
        );

        $this->activateSubDataSerializer();
        return $this->manager->createData($resource)->toArray();
    }
}
