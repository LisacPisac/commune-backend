<?php

namespace App\Http\Resources;

use App\Entities\Users;
use App\Http\Transformers\UserTransformer;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;

class UserResource extends AbstractResource
{

    public function make(Users $user): string
    {
        $resource = new FractalItem(
            $user,
            new UserTransformer()
        );

        $this->manager->setSerializer($this->dataArraySerializer);
        return $this->manager->createData($resource)->toJson();
    }

    public function collect($users): string
    {
        $resource = new FractalCollection(
            $users,
            new UserTransformer()
        );

        $this->manager->setSerializer($this->dataArraySerializer);
        return $this->manager->createData($resource)->toJson();
    }

    public function makeForResource(Users $user): array
    {
        $resource = new FractalItem(
            $user,
            new UserTransformer()
        );

        $this->manager->setSerializer($this->arraySerializer);
        return $this->manager->createData($resource)->toArray();
    }

    public function collectForResource($users): array
    {
        $resource = new FractalCollection(
            $users,
            new UserTransformer()
        );

        $this->activateSubDataSerializer();
        return $this->manager->createData($resource)->toArray();
    }
}
