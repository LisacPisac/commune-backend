<?php

namespace App\Http\Resources;

use App\Entities\Likes;
use App\Http\Transformers\LikeTransformer;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;

class LikeResource extends AbstractResource
{
    public function make(Likes $like): string
    {
        $resource = new FractalItem(
            $like,
            new LikeTransformer()
        );

        $this->activateDataArraySerializer();
        return $this->manager->createData($resource)->toJson();
    }

    public function collect($likes): string
    {
        $resource = new FractalCollection(
            $likes,
            new LikeTransformer()
        );

        $this->activateDataArraySerializer();
        return $this->manager->createData($resource)->toJson();
    }

    public function collectForResource($likes): array
    {
        $resource = new FractalCollection(
            $likes,
            new LikeTransformer()
        );

        $this->activateSubDataSerializer();
        return $this->manager->createData($resource)->toArray();
    }
}
