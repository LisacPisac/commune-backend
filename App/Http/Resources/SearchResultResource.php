<?php

namespace App\Http\Resources;

use App\Http\Transformers\SearchResultTransformer;
use App\Models\SearchResult;
use League\Fractal\Resource\Item as FractalItem;

class SearchResultResource extends AbstractResource
{
    public function make(SearchResult $searchResult)
    {
        $resource = new FractalItem(
            $searchResult,
            new SearchResultTransformer()
        );

        return $this->manager->createData($resource)->toJson();
    }
}