<?php

namespace App\Http\Resources;

use App\Entities\Comments;
use App\Http\Transformers\CommentTransformer;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;

class CommentResource extends AbstractResource
{
    public function make(Comments $comment): string
    {
        $resource = new FractalItem(
            $comment,
            new CommentTransformer()
        );

        $this->activateDataArraySerializer();
        return $this->manager->createData($resource)->toJson();
    }

    public function collect($comments): string
    {
        $resource = new FractalCollection(
            [
                $comments,
            ],
            new CommentTransformer()
        );

        $this->activateDataArraySerializer();
        return $this->manager->createData($resource)->toJson();
    }

    public function makeForResource(Comments $comment): array
    {
        $resource = new FractalItem(
            $comment,
            new CommentTransformer()
        );

        $this->activateSubDataSerializer();
        return $this->sendArrayData($resource)[0];
    }

    public function collectForResource($comments): array
    {
        $resource = new FractalCollection(
            $comments,
            new CommentTransformer()
        );

        $this->activateSubDataSerializer();
        return $this->sendArrayData($resource);
    }
}
