<?php

namespace App\Http\Resources;

use App\Entities\Orders;
use App\Http\Transformers\OrderTransformer;
use League\Fractal\Resource\Item as FractalItem;

class OrderResource extends AbstractResource
{
    public function make(Orders $order)
    {
        $resource = new FractalItem(
            $order,
            new OrderTransformer
        );

        $this->activateDataArraySerializer();
        return $this->sendJsonData($resource);
    }
}
