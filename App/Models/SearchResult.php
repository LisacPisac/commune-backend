<?php

namespace App\Models;

class SearchResult
{
    public $users;
    public $groups;
    public $posts;

    public function fromArray(array $data)
    {
        $this->users = $data['users'];
        $this->groups = $data['groups'];
        $this->posts = $data['posts'];

        return $this;
    }

    public function toArray()
    {
        return [
            'users' => $this->users,
            'groups' => $this->groups,
            'posts' => $this->posts,
        ];
    }
}