<?php

namespace App\Models\Billing;

class PaypalPurchaseUnitsAmount
{
    public string $currencyCode;
    public float $value;

    public function __construct($paypalPurchaseUnitsAmount)
    {
        $this->currencyCode = $paypalPurchaseUnitsAmount->currency_code;
        $this->value = $paypalPurchaseUnitsAmount->value;
    }

}
