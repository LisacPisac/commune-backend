<?php

namespace App\Models\Billing;

class PaypalCreateOrderResponse
{
    public string $id;
    public string $intent;
    public string $status;

    /** @var PaypalPurchaseUnits[] $purchaseUnits */
    public $purchaseUnits;
    public string $createTime;

    /** @var PaypalPaymentLink[] */
    public $links;

    public array $headers;

    public function __construct(object $result, array $headers)
    {
        $this->id = $result->id;
        $this->intent = $result->intent;
        $this->status = $result->status;
        $this->purchaseUnits = new PaypalPurchaseUnits($result->purchase_units[0]);
        $this->createTime = $result->create_time;
        $this->links = $result->links;
        $this->headers = $headers;
    }
}
