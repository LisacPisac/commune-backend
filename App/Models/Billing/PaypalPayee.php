<?php

namespace App\Models\Billing;

class PaypalPayee
{
    public string $emailAddress;
    public string $merchantId;

    public function __construct($paypalPayee)
    {
        $this->emailAddress = $paypalPayee->email_address;
        $this->merchantId = $paypalPayee->merchant_id;
    }

}
