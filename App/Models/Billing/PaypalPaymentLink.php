<?php

namespace App\Models\Billing;

class PaypalPaymentLink
{
    public string $href;
    public string $rel;
    public string $method;
}
