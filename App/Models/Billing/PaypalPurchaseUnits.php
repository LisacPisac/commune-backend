<?php

namespace App\Models\Billing;

class PaypalPurchaseUnits
{
    public string $referenceId;
    public PaypalPurchaseUnitsAmount $amount;
    public PaypalPayee $payee;
    public string $description;

    public function __construct($purchaseUnits)
    {
        $this->referenceId = $purchaseUnits->reference_id;
        $this->amount = new PaypalPurchaseUnitsAmount($purchaseUnits->amount);
        $this->payee = new PaypalPayee($purchaseUnits->payee);
        $this->description = $purchaseUnits->description;
    }
}
