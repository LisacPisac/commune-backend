<?php

namespace App\Models;

class TfaSecret
{
    public string $secret;
    public string $qrCode;
    public string $qrText;

    public function __construct(string $secret, string $qrCode, string $qrText)
    {
        $this->secret = $secret;
        $this->qrCode = $qrCode;
        $this->qrText = $qrText;
    }

    public function toArray(): array
    {
        return [
            'secret'  => $this->secret,
            'qr_code' => $this->qrCode,
            'qr_text' => $this->qrText,
        ];
    }

    public function fromArray(array $data): TfaSecret
    {
        $this->qrCode = $data['qr_code'];
        $this->secret = $data['secret'];
        $this->qrText = $data['qr_text'];

        return $this;
    }
}
