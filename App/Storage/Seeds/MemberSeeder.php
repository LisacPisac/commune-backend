<?php

use App\Enums\DbTableNamesEnum;
use Phinx\Seed\AbstractSeed;

class MemberSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $members = $this->table(DbTableNamesEnum::MEMBERS);

        $members->insert(
            [
                [
                    'group_id' => 1,
                    'user_id'  => 1,
                    'admin'    => true,
                    'pending'  => false,
                ],
                [
                    'group_id' => 1,
                    'user_id'  => 2,
                    'admin'    => false,
                    'pending'  => false,
                ],
                [
                    'group_id' => 1,
                    'user_id'  => 3,
                    'admin'    => false,
                    'pending'  => false,
                ],
                [
                    'group_id' => 1,
                    'user_id'  => 4,
                    'admin'    => false,
                    'pending'  => true,
                ],
            ]
        )
            ->saveData();
    }
}
