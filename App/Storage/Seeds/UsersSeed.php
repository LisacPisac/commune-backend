<?php

use App\Enums\DbTableNamesEnum;
use Phinx\Seed\AbstractSeed;

class UsersSeed extends AbstractSeed
{
    public function run()
    {
        $security = new \Phalcon\Security();
        $table = $this->table(DbTableNamesEnum::USERS);
        $table->insert(
            [
                [
                    'username' => 'Rincewind',
                    'subtitle' => 'Wizzard',
                    'email'    => 'test@test.com',
                    'password' => $security->hash('test'),
                    'enabled'  => true,
                ],
                [
                    'username' => 'Brutha',
                    'subtitle' => 'Chosen one',
                    'email'    => 'test2@test.com',
                    'password' => $security->hash('test2'),
                    'enabled'  => true,
                ],
                [
                    'username' => 'Lord Vetinari',
                    'subtitle' => 'Patrician',
                    'email'    => 'test3@test.com',
                    'password' => $security->hash('test3'),
                    'enabled'  => true,
                ],
                [
                    'username' => 'Sam Vines',
                    'subtitle' => 'Cpt. of the Guard',
                    'email'    => 'test4@test.com',
                    'password' => $security->hash('test4'),
                    'enabled'  => true,
                ],
            ]
        )->saveData();
    }
}
