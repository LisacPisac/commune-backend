<?php

use App\Enums\DbTableNamesEnum;
use Phinx\Seed\AbstractSeed;

class GroupsSeed extends AbstractSeed
{
    public function getDependencies()
    {
        return [
            'UsersSeed',
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $table = $this->table(DbTableNamesEnum::GROUPS);
        $table->insert(
            [
                [
                    'name' => 'General',
                    'owner' => 1,
                ],
            ]
        )->saveData();
    }
}
