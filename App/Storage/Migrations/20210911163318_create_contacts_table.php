<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class CreateContactsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table(
            DbTableNamesEnum::CONTACTS,
            ['id' => false, 'primary_key' => ['user_a', 'user_b']]
        );
        $table
            ->addColumn(
                'user_a',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::NON_NULLABLE,
                    DbMigrationOptionsEnum::FOREIGN_KEY
                )
            )
            ->addColumn(
                'user_b',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::NON_NULLABLE,
                    DbMigrationOptionsEnum::FOREIGN_KEY
                )
            )
            ->addColumn(
                'pending',
                AdapterInterface::PHINX_TYPE_BOOLEAN,
                DbMigrationOptionsEnum::DEFAULT_TRUE
            )
            ->addForeignKey(
                'user_a',
                DbTableNamesEnum::USERS,
                'id',
                DbMigrationOptionsEnum::FK_CASCADE
            )
            ->addForeignKey(
                'user_b',
                DbTableNamesEnum::USERS,
                'id',
                DbMigrationOptionsEnum::FK_CASCADE
            )
            ->create();
    }
}
