<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class CreatePostFilesTable extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table(
            DbTableNamesEnum::POST_FILES,
            DbMigrationOptionsEnum::PRIMARY_KEY
        );
        $table
            ->addColumn(
                'post_id',
                AdapterInterface::PHINX_TYPE_INTEGER,
                DbMigrationOptionsEnum::FOREIGN_KEY
            )
            ->addColumn(
                'file_id',
                AdapterInterface::PHINX_TYPE_INTEGER,
                DbMigrationOptionsEnum::FOREIGN_KEY
            )
            ->addForeignKey(
                'post_id',
                DbTableNamesEnum::POSTS,
                'id',
                DbMigrationOptionsEnum::FK_CASCADE
            )
            ->addForeignKey(
                'file_id',
                DbTableNamesEnum::FILES,
                'id',
                DbMigrationOptionsEnum::FK_CASCADE
            )
            ->create();
    }
}
