<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class CreatePostsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $postsTable = $this->table(
            DbTableNamesEnum::POSTS,
            DbMigrationOptionsEnum::PRIMARY_KEY
        );
        $postsTable
            ->addColumn(
                'text',
                AdapterInterface::PHINX_TYPE_TEXT,
                DbMigrationOptionsEnum::NULLABLE
            )
            ->addColumn(
                'author',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::FOREIGN_KEY,
                    DbMigrationOptionsEnum::NULLABLE

                )
            )
            ->addColumn(
                'group_id',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::FOREIGN_KEY,
                    DbMigrationOptionsEnum::NULLABLE

                )
            )
            ->addColumn(
                'tags',
                AdapterInterface::PHINX_TYPE_JSON,
                DbMigrationOptionsEnum::NULLABLE
            )
            ->addForeignKey(
                'author',
                DbTableNamesEnum::USERS,
                'id',
                DbMigrationOptionsEnum::FK_SET_NULL
            )
            ->addForeignKey(
                'group_id',
                DbTableNamesEnum::GROUPS,
                'id',
                DbMigrationOptionsEnum::FK_SET_NULL
            )
            ->addTimestamps()
            ->create();
    }
}
