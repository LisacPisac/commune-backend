<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class AddHeaderFileToGroup extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table(DbTableNamesEnum::GROUPS);

        $table->addColumn(
            'group_cover_file_id',
            AdapterInterface::PHINX_TYPE_INTEGER,
            array_merge(
                DbMigrationOptionsEnum::NULLABLE,
                DbMigrationOptionsEnum::FOREIGN_KEY
            )
        )
            ->addForeignKey(
                'group_cover_file_id',
                DbTableNamesEnum::FILES,
                'id',
                DbMigrationOptionsEnum::FK_CASCADE
            )
            ->update();
    }
}
