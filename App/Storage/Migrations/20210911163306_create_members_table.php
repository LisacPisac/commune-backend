<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class CreateMembersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table(
            DbTableNamesEnum::MEMBERS,
            ['id' => false, 'primary_key' => ['group_id', 'user_id']]
        );
        $table
            ->addColumn(
                'group_id',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::NON_NULLABLE,
                    DbMigrationOptionsEnum::FOREIGN_KEY
                )
            )
            ->addColumn(
                'user_id',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::NON_NULLABLE,
                    DbMigrationOptionsEnum::FOREIGN_KEY
                )
            )
            ->addColumn(
                'admin',
                AdapterInterface::PHINX_TYPE_BOOLEAN,
                DbMigrationOptionsEnum::NON_NULLABLE
            )
            ->addColumn(
                'pending',
                AdapterInterface::PHINX_TYPE_BOOLEAN,
                array_merge(
                    DbMigrationOptionsEnum::NON_NULLABLE,
                    ['default' => 1]
                )
            )
            ->addForeignKey(
                'group_id',
                DbTableNamesEnum::GROUPS,
                'id',
                DbMigrationOptionsEnum::FK_CASCADE
            )
            ->addForeignKey(
                'user_id',
                DbTableNamesEnum::USERS,
                'id',
                DbMigrationOptionsEnum::FK_CASCADE
            )
            ->create();
    }
}
