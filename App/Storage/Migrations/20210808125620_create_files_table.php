<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class CreateFilesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $filesTable = $this->table(
            DbTableNamesEnum::FILES,
            DbMigrationOptionsEnum::PRIMARY_KEY
        );
        $filesTable
//            ->addColumn(
//                'file',
//                AdapterInterface::PHINX_TYPE_LONGBLOB,
//                DbMigrationOptionsEnum::NON_NULLABLE
//            )
            ->addColumn(
                'file',
                AdapterInterface::PHINX_TYPE_TEXT,
                DbMigrationOptionsEnum::NON_NULLABLE
            )
            ->addColumn(
                'type',
                AdapterInterface::PHINX_TYPE_STRING,
                DbMigrationOptionsEnum::STRING_255_NON_NULLABLE
            )
            ->addTimestamps()
            ->create();
    }
}
