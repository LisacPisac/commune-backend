<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class CreateGroupsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $groupsTable = $this->table(
            DbTableNamesEnum::GROUPS,
            DbMigrationOptionsEnum::PRIMARY_KEY
        );
        $groupsTable
            ->addColumn(
                'name',
                AdapterInterface::PHINX_TYPE_STRING,
                DbMigrationOptionsEnum::STRING_255_NON_NULLABLE
            )
            ->addColumn(
                'owner',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::NON_NULLABLE,
                    DbMigrationOptionsEnum::FOREIGN_KEY
                )
            )
            ->addColumn(
                'group_avatar_file_id',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::NULLABLE,
                    DbMigrationOptionsEnum::FOREIGN_KEY
                )
            )
            ->addForeignKey('owner', DbTableNamesEnum::USERS)
            ->addForeignKey('group_avatar_file_id', DbTableNamesEnum::FILES)
            ->addTimestamps()
            ->create();
    }
}
