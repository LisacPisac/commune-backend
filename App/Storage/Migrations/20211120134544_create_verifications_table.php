<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class CreateVerificationsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table(DbTableNamesEnum::VERIFICATIONS, DbMigrationOptionsEnum::PRIMARY_KEY);
        $table
            ->addColumn(
                'token',
                AdapterInterface::PHINX_TYPE_TEXT,
                DbMigrationOptionsEnum::NON_NULLABLE
            )
            ->addColumn(
                'user_id',
                AdapterInterface::PHINX_TYPE_INTEGER,
                DbMigrationOptionsEnum::FOREIGN_KEY
            )
            ->addTimestamps()
            ->addForeignKey(
                'user_id',
                DbTableNamesEnum::USERS,
                'id',
                DbMigrationOptionsEnum::FK_CASCADE
            )
            ->create();
    }
}
