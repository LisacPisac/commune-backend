<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class CreateUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $usersTable = $this->table(
            DbTableNamesEnum::USERS,
            DbMigrationOptionsEnum::PRIMARY_KEY
        );
        try {
            $usersTable
                ->addColumn(
                    'username',
                    AdapterInterface::PHINX_TYPE_STRING,
                    DbMigrationOptionsEnum::STRING_255_NON_NULLABLE
                )
                ->addColumn(
                    'password',
                    AdapterInterface::PHINX_TYPE_STRING,
                    DbMigrationOptionsEnum::STRING_255_NON_NULLABLE
                )
                ->addColumn(
                    'email',
                    AdapterInterface::PHINX_TYPE_STRING,
                    DbMigrationOptionsEnum::STRING_255_NON_NULLABLE
                )
                ->addColumn(
                    'enabled',
                    AdapterInterface::PHINX_TYPE_BOOLEAN,
                    DbMigrationOptionsEnum::DEFAULT_TRUE
                )
                ->addColumn(
                    'avatar_file_id',
                    AdapterInterface::PHINX_TYPE_INTEGER,
                    array_merge(
                        DbMigrationOptionsEnum::NULLABLE,
                        DbMigrationOptionsEnum::FOREIGN_KEY
                    )
                )
                ->addColumn(
                    'summary',
                    AdapterInterface::PHINX_TYPE_STRING,
                    [
                        'limit' => 1000,
                        'null'  => true,
                    ]
                )
                ->addColumn(
                    'subtitle',
                    AdapterInterface::PHINX_TYPE_STRING,
                    DbMigrationOptionsEnum::STRING_255_NULLABLE
                )
                ->addForeignKey(
                    'avatar_file_id',
                    DbTableNamesEnum::FILES
                )
                ->create();
        } catch (Throwable $throwable) {
            $this->getOutput()->writeln($throwable->getMessage());
        }
    }
}
