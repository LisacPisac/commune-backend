<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class CreateMessagesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table(DbTableNamesEnum::MESSAGES);
        $table
            ->addColumn(
                'text',
                AdapterInterface::PHINX_TYPE_STRING
            )
            ->addColumn(
                'read',
                AdapterInterface::PHINX_TYPE_BOOLEAN,
                array_merge(
                    DbMigrationOptionsEnum::NON_NULLABLE,
                    ['default' => 0]
                )
            )
            ->addColumn(
                'chat_id',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::FOREIGN_KEY,
                    DbMigrationOptionsEnum::NON_NULLABLE
                )
            )
            ->addColumn(
                'user_id',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::FOREIGN_KEY,
                    DbMigrationOptionsEnum::NON_NULLABLE
                )
            )
            ->addTimestamps()
            ->addForeignKey(
                'chat_id',
                DbTableNamesEnum::CHATS
            )
            ->addForeignKey(
                'user_id',
                DbTableNamesEnum::USERS
            )
            ->create();
    }
}
