<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class CreateSessionsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        /** Query as per https://github.com/phalcon/incubator/tree/master/Library/Phalcon/Session/Adapter#database */
//        $this->query(
//            <<<SQL
//            CREATE TABLE `sessions` (
//                `session_id` VARCHAR(35) NOT NULL,
//                `data` text NOT NULL,
//                `created_at` INT unsigned NOT NULL,
//                `modified_at` INT unsigned DEFAULT NULL,
//                PRIMARY KEY (`session_id`)
//            );
//        SQL

        $table = $this->table(
            DbTableNamesEnum::SESSIONS,
            ['id' => false, 'primary_key' => 'session_id']
        );
        $table
            ->addColumn(
                'session_id',
                AdapterInterface::PHINX_TYPE_STRING,
                [
                    'limit' => 35,
                    'null'  => false,
                ]
            )
            ->addColumn(
                'data',
                AdapterInterface::PHINX_TYPE_TEXT,
                DbMigrationOptionsEnum::NON_NULLABLE
            )
            ->addTimestamps()
            ->create();
    }
}
