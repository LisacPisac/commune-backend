<?php
declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use App\Enums\PostPrivacySettingsEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class AddPrivacySettingsToPosts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table(DbTableNamesEnum::POSTS);
        $table
            ->addColumn(
                'privacy',
                AdapterInterface::PHINX_TYPE_STRING,
                array_merge(
                    DbMigrationOptionsEnum::STRING_255_NON_NULLABLE,
                    ['default' => PostPrivacySettingsEnum::EVERYONE]
                )
            )
            ->update();
    }
}
