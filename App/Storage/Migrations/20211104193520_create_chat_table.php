<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class CreateChatTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table(DbTableNamesEnum::CHATS, DbMigrationOptionsEnum::PRIMARY_KEY);
        $table
            ->addColumn(
                'users',
                AdapterInterface::PHINX_TYPE_JSON,
                DbMigrationOptionsEnum::NON_NULLABLE
            )
            ->addColumn(
                'name',
                AdapterInterface::PHINX_TYPE_STRING,
                DbMigrationOptionsEnum::STRING_255_NULLABLE
            )
            ->addTimestamps()
            ->create();
    }
}
