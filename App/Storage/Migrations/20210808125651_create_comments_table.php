<?php

declare(strict_types=1);

use App\Enums\DbMigrationOptionsEnum;
use App\Enums\DbTableNamesEnum;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Migration\AbstractMigration;

final class CreateCommentsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $commentsTable = $this->table(
            DbTableNamesEnum::COMMENTS,
            DbMigrationOptionsEnum::PRIMARY_KEY
        );
        $commentsTable
            ->addColumn(
                'text',
                AdapterInterface::PHINX_TYPE_TEXT,
                DbMigrationOptionsEnum::NON_NULLABLE
            )
            ->addColumn(
                'post_id',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::NON_NULLABLE,
                    DbMigrationOptionsEnum::FOREIGN_KEY

                )
            )
            ->addColumn(
                'author',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::NULLABLE,
                    DbMigrationOptionsEnum::FOREIGN_KEY
                )
            )
            ->addColumn(
                'comment_id',
                AdapterInterface::PHINX_TYPE_INTEGER,
                array_merge(
                    DbMigrationOptionsEnum::NULLABLE,
                    DbMigrationOptionsEnum::FOREIGN_KEY
                )
            )
            ->addColumn(
                'is_deleted',
                AdapterInterface::PHINX_TYPE_BOOLEAN,
                [
                    'default' => 0,
                    'null'    => false,
                ]
            )
            ->addTimestamps()
            ->addForeignKey('post_id', DbTableNamesEnum::POSTS)
            ->addForeignKey('comment_id', DbTableNamesEnum::COMMENTS)
            ->addForeignKey(
                'author',
                DbTableNamesEnum::USERS,
                ['id'],
                DbMigrationOptionsEnum::FK_CASCADE
            )
            ->create();
    }
}
