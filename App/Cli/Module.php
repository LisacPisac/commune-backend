<?php

namespace App\Cli;

use Phalcon\Di\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{
    /**
     * Registers an autoloader related to the module
     *
     * @param  DiInterface  $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();

        $loader->registerNamespaces([
            'App\Cli' => __DIR__,
        ]);

        $loader->register();
    }

    /**
     * Registers services related to the module
     *
     * @param  DiInterface  $di
     */
    public function registerServices(DiInterface $di)
    {
    }
}
