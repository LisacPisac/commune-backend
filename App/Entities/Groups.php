<?php

namespace App\Entities;

class Groups extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var integer
     */
    protected $owner;

    /**
     *
     * @var integer
     */
    protected $group_avatar_file_id;

    /**
     *
     * @var string
     */
    protected $created_at;

    /**
     *
     * @var string
     */
    protected $updated_at;

    /**
     *
     * @var integer
     */
    protected $group_cover_file_id;

    /**
     * Method to set the value of field id
     *
     * @param  integer  $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param  string  $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field owner
     *
     * @param  integer  $owner
     * @return $this
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Method to set the value of field group_avatar_file_id
     *
     * @param  integer  $group_avatar_file_id
     * @return $this
     */
    public function setGroupAvatarFileId($group_avatar_file_id)
    {
        $this->group_avatar_file_id = $group_avatar_file_id;

        return $this;
    }

    /**
     * Method to set the value of field created_at
     *
     * @param  string  $created_at
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Method to set the value of field updated_at
     *
     * @param  string  $updated_at
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Method to set the value of field group_cover_file_id
     *
     * @param  integer  $group_cover_file_id
     * @return $this
     */
    public function setGroupCoverFileId($group_cover_file_id)
    {
        $this->group_cover_file_id = $group_cover_file_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field owner
     *
     * @return integer
     */
    public function getOwner()
    {
        return intval($this->owner);
    }

    /**
     * Returns the value of field group_avatar_file_id
     *
     * @return integer
     */
    public function getGroupAvatarFileId()
    {
        return $this->group_avatar_file_id;
    }

    /**
     * Returns the value of field created_at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Returns the value of field updated_at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Returns the value of field group_cover_file_id
     *
     * @return integer
     */
    public function getGroupCoverFileId()
    {
        return $this->group_cover_file_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("commune");
        $this->setSource("groups");
        $this->belongsTo(
            'owner',
            'App\Entities\Users',
            'id',
            ['alias' => 'Users']
        );
        $this->belongsTo(
            'group_avatar_file_id',
            'App\Entities\Files',
            'id',
            ['alias' => 'AvatarFile']
        );
        $this->belongsTo(
            'group_cover_file_id',
            'App\Entities\Files',
            'id',
            ['alias' => 'CoverFile']
        );
        $this->hasMany(
            'id',
            'App\Entities\Members',
            'group_id',
            ['alias' => 'Members']
        );
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param  mixed  $parameters
     * @return Groups[]|Groups|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param  mixed  $parameters
     * @return Groups|\Phalcon\Mvc\Model\ResultInterface|\Phalcon\Mvc\ModelInterface|null
     */
    public static function findFirst($parameters = null): ?\Phalcon\Mvc\ModelInterface
    {
        return parent::findFirst($parameters);
    }

}
