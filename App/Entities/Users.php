<?php

namespace App\Entities;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class Users extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $username;

    /**
     *
     * @var string
     */
    protected $password;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var integer
     */
    protected $enabled;

    /**
     *
     * @var integer
     */
    protected $avatar_file_id;

    /**
     *
     * @var string
     */
    protected $summary;

    /**
     *
     * @var string
     */
    protected $subtitle;

    /**
     *
     * @var string
     */
    protected $tfa_secret;

    /**
     *
     * @var integer
     */
    protected $is_deleted;

    /**
     * Method to set the value of field id
     *
     * @param  integer  $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field username
     *
     * @param  string  $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Method to set the value of field password
     *
     * @param  string  $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param  string  $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field enabled
     *
     * @param  integer  $enabled
     * @return $this
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Method to set the value of field avatar_file_id
     *
     * @param  integer  $avatar_file_id
     * @return $this
     */
    public function setAvatarFileId($avatar_file_id)
    {
        $this->avatar_file_id = $avatar_file_id;

        return $this;
    }

    /**
     * Method to set the value of field summary
     *
     * @param  string  $summary
     * @return $this
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Method to set the value of field subtitle
     *
     * @param  string  $subtitle
     * @return $this
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Method to set the value of field tfa_secret
     *
     * @param  string  $tfa_secret
     *
     * @return $this
     */
    public function setTfaSecret($tfa_secret)
    {
        $this->tfa_secret = $tfa_secret;

        return $this;
    }

    /**
     * Method to set the value of field is_deleted
     *
     * @param  integer  $is_deleted
     *
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return intval($this->id);
    }

    /**
     * Returns the value of field username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Returns the value of field password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field enabled
     *
     * @return integer
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Returns the value of field avatar_file_id
     *
     * @return integer
     */
    public function getAvatarFileId()
    {
        return $this->avatar_file_id;
    }

    /**
     * Returns the value of field summary
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Returns the value of field subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Returns the value of field tfa_secret
     *
     * @return string
     */
    public function getTfaSecret()
    {
        return $this->tfa_secret;
    }

    /**
     * Returns the value of field is_deleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return intval($this->is_deleted) === 1;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'model'   => $this,
                    'message' => 'Please enter a correct email address',
                ]
            )
        );

        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("commune");
        $this->setSource("users");
        $this->hasMany(
            'id',
            'App\Entities\Comments',
            'author',
            ['alias' => 'Comments']
        );
        $this->hasMany(
            'id',
            'App\Entities\Contacts',
            'user_a',
            [
                'alias'  => 'Contacts',
                'params' => [
                    'conditions' => 'pending IS 0',
                ],
            ]
        );
        $this->hasMany(
            'id',
            'App\Entities\Contacts',
            'user_b',
            [
                'alias'  => 'Contacts',
                'params' => [
                    'conditions' => 'pending IS 0',
                ],
            ]
        );
        $this->hasMany(
            'id',
            'App\Entities\Groups',
            'owner',
            ['alias' => 'Groups']
        );
        $this->hasMany(
            'id',
            'App\Entities\Likes',
            'user_id',
            ['alias' => 'Likes']
        );
        $this->hasMany(
            'id',
            'App\Entities\Members',
            'user_id',
            ['alias' => 'Members']
        );
        $this->hasMany(
            'id',
            'App\Entities\Posts',
            'author',
            ['alias' => 'Posts']
        );
        $this->belongsTo(
            'avatar_file_id',
            'App\Entities\Files',
            'id',
            ['alias' => 'Files']
        );
        $this->hasOne(
            'id',
            'App\Entities\Orders',
            'user_id',
            ['alias' => 'Orders']
        );
    }

    public function isPremium()
    {
        /** @var Orders $order */
        $order = $this->getOrders();

        if (empty($order)) {
            return false;
        } else {
            return $order->getStatus() === Orders::STATUS_APPROVED;
        }
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param  mixed  $parameters
     * @return Users[]|Users|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param  mixed  $parameters
     * @return Users|\Phalcon\Mvc\Model\ResultInterface|\Phalcon\Mvc\ModelInterface|null
     */
    public static function findFirst($parameters = null): ?\Phalcon\Mvc\ModelInterface
    {
        return parent::findFirst($parameters);
    }

}
