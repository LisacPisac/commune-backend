<?php

namespace App\Entities;

class Contacts extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $user_a;

    /**
     *
     * @var integer
     */
    protected $user_b;

    /**
     *
     * @var integer
     */
    protected $pending;

    /**
     * Method to set the value of field user_a
     *
     * @param  integer  $user_a
     *
     * @return $this
     */
    public function setUserA($user_a)
    {
        $this->user_a = $user_a;

        return $this;
    }

    /**
     * Method to set the value of field user_b
     *
     * @param  integer  $user_b
     * @return $this
     */
    public function setUserB($user_b)
    {
        $this->user_b = $user_b;

        return $this;
    }

    /**
     * Method to set the value of field pending
     *
     * @param  integer  $pending
     * @return $this
     */
    public function setPending($pending)
    {
        $this->pending = $pending;

        return $this;
    }

    /**
     * Returns the value of field user_a
     *
     * @return integer
     */
    public function getUserA()
    {
        return $this->user_a;
    }

    /**
     * Returns the value of field user_b
     *
     * @return integer
     */
    public function getUserB()
    {
        return $this->user_b;
    }

    /**
     * Returns the value of field pending
     *
     * @return integer
     */
    public function getPending()
    {
        return $this->pending;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("commune");
        $this->setSource("contacts");
        $this->belongsTo(
            'user_a',
            'App\Entities\Users',
            'id',
            ['alias' => 'UserAEntity']
        );
        $this->belongsTo(
            'user_b',
            'App\Entities\Users',
            'id',
            ['alias' => 'UserBEntity']
        );
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param  mixed  $parameters
     * @return Contacts[]|Contacts|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param  mixed  $parameters
     * @return Contacts|\Phalcon\Mvc\Model\ResultInterface|\Phalcon\Mvc\ModelInterface|null
     */
    public static function findFirst($parameters = null): ?\Phalcon\Mvc\ModelInterface
    {
        return parent::findFirst($parameters);
    }

}
