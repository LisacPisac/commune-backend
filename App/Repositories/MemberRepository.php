<?php

namespace App\Repositories;

use App\Entities\Members;
use Phalcon\Di\DiInterface;
use Phalcon\Mvc\Model\Query;

class MemberRepository
{
    private DiInterface $container;

    public function __construct(DiInterface $container)
    {
        $this->container = $container;
    }

    public function createPending(int $userId, int $groupId): Members
    {
        $member = new Members();
        $member
            ->setUserId($userId)
            ->setGroupId($groupId)
            ->setAdmin(0)
            ->setPending(1)
            ->save();

        return $member->refresh();
    }

    public function createActive(int $userId, int $groupId): Members
    {
        $member = new Members();
        $member
            ->setUserId($userId)
            ->setGroupId($groupId)
            ->setAdmin(0)
            ->setPending(0)
            ->save();

        return $member->refresh();
    }

    public function setAdmin(int $userId, int $groupId)
    {
        $member = $this->find($userId, $groupId);
        $member->setAdmin(true);

        return $member->refresh();
    }

    public function setActive(int $userId, int $groupId)
    {
        $member = $this->find($userId, $groupId);
        $member->setPending(false);

        return $member->refresh();
    }

    public function find(int $userId, int $groupId): ?Members
    {
        return Members::findFirst(
            [
                'user_id = :user_id: AND group_id = :group_id:',
                'bind' => [
                    'group_id' => $groupId,
                    'user_id'  => $userId,
                ],
            ]
        );
    }

    public function revokeAdmin(int $userId, int $groupId)
    {
        $member = $this->find($userId, $groupId);
        $member->setAdmin(false);

        return $member->refresh();
    }

    public function getPending(int $groupId)
    {
        return Members::find("group_id = $groupId AND pending = true");
    }

    public function isAdmin(int $userId, int $groupId)
    {
        return $this->find($userId, $groupId)->getAdmin();
    }

    public function delete(int $userId, int $groupId)
    {
        return $this->find($userId, $groupId)->delete();
    }

    public function update(int $userId, int $groupId, array $parameters)
    {
        $member = $this->find($userId, $groupId);

        $member->assign($parameters)->save();

        return $member->refresh();
    }

    // public function getByGroup(int $groupId)
    // {
    //     return Members::find("group_id = $groupId AND pending = false");
    // }

    public function getByGroup(int $groupId)
    {
        $phql
            = "SELECT members.* FROM App\Entities\Members as members LEFT JOIN App\Entities\Users as users ON users.id = members.user_id WHERE members.group_id = $groupId AND users.is_deleted = 0";
        $query = new Query(
            $phql,
            $this->container
        );

        /** @var Simple $posts */
        return $query->execute();
    }
}
