<?php

namespace App\Repositories;

use App\Entities\Comments;
use App\Entities\Users;
use App\Exceptions\UnauthorisedException;
use Monolog\Logger;
use Phalcon\Di\DiInterface;

class CommentRepository
{
    private DiInterface $container;

    public function __construct(DiInterface $container)
    {
        $this->container = $container;
    }

    public function find(int $commentId)
    {
        return Comments::find($commentId)->getFirst();
    }

    public function create(int $userId, array $parameters): Comments
    {
        $comment = new Comments();

        if (array_key_exists('comment_id', $parameters)) {
            $replyComment = $this->find(intval($parameters['comment_id']));
            $parameters['post_id'] = $replyComment->getPostId();
        }

        $success = $comment->setAuthor($userId)->assign($parameters)->save();
        if ( ! $success) {
            $context = [];
            foreach ($comment->getMessages() as $message) {
                $context[] = [
                    $message->getCode(),
                    $message->getField(),
                    $message->getMessage(),
                    $message->getMetaData(),
                ];
            }

            /** @var Logger $logger */
            $logger = $this->container->getShared('logger');
            $logger->error(
                'Error while creating Comment!',
                $context
            );
        }

        return $comment->refresh();
    }

    public function update(
        Users $authUser,
        Comments $comment,
        array $parameters
    ): Comments {
        if ($authUser->getId() !== $comment->getAuthor()) {
            throw new UnauthorisedException();
        }

        $comment->refresh();

        $comment->setText($parameters['text']);

        return $comment->refresh();
    }
}
