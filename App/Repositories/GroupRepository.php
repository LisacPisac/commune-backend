<?php

namespace App\Repositories;

use App\Entities\Groups;
use Phalcon\Di\DiInterface;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\ResultsetInterface;

class GroupRepository
{
    private DiInterface $container;

    public function __construct()
    {
        $this->container = FactoryDefault::getDefault();
    }

    public function find(int $groupId): Groups
    {
        return Groups::find($groupId)->getFirst();
    }

    public function search(string $searchTerm): ResultsetInterface
    {
        return Groups::query(FactoryDefault::getDefault())
            ->where("name LIKE '%$searchTerm%'")
            ->orderBy('name ASC')
            ->execute();
    }

    public function get(array $parameters = [])
    {
        $limitClause = $this->buildLimitClause($parameters);

        $phql
            = "SELECT * FROM App\Entities\Groups ORDER BY created_at $limitClause";
        $query = new Query(
            $phql,
            $this->container
        );

        /** @var Simple $posts */
        return $query->execute();
    }

    public function create(int $ownerId, string $name): Groups
    {
        $group = new Groups();
        $group->setName($name);
        $group->setOwner($ownerId);
        $group->save();

        return $group->refresh();
    }

    public function update(int $groupId, array $parameters)
    {
        $group = $this->find($groupId);

        $group->assign($parameters)->save();

        return $group->refresh();
    }

    public function delete(int $groupId)
    {
        $group = $this->find($groupId);

        return $group->delete();
    }

    private function buildWhereClause(array $parameters): string
    {
        $clause = "";

        foreach ($parameters as $key => $value) {
            $clause .= " $key = :$key:";
        }

        return $clause;
    }

    private function buildLimitClause(array $parameters): string
    {
        $limit = intval($parameters['limit']);
        $page = intval($parameters['page']);

        return " LIMIT $limit OFFSET " . (($page - 1) * $limit);
    }
}
