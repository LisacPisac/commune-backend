<?php

namespace App\Repositories;

use App\Entities\Posts;
use App\Entities\Users;
use Monolog\Logger;
use Phalcon\Di\DiInterface;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple;

class PostRepository
{
    private DiInterface $container;

    public function __construct()
    {
        $this->container = FactoryDefault::getDefault();
    }

    public function findByParameters(array $parameters)
    {
        $queryParams = [];

        foreach ($parameters as $key => $value) {
            $queryParams[] = "$key = $value";
        }

        return Posts::find(
            $queryParams
        );
    }

    public function search(string $searchTerm)
    {
        return Posts::query(FactoryDefault::getDefault())
            ->where("text LIKE '%$searchTerm%'")
            ->execute();
    }

    public function searchByTag(string $searchTerm)
    {
        return Posts::query(FactoryDefault::getDefault())
            ->where("JSON_CONTAINS(tags, '\"$searchTerm\"', '$') = 1")
            ->execute();
    }

    public function searchByAuthor(string $username)
    {
        return Posts::query(FactoryDefault::getDefault())
            ->innerJoin(Users::class, "u.id = author", "u")
            ->where("u.username = '$username'")
            ->where('is_deleted = 0')
            ->execute();
    }

    public function create(int $userId, array $parameters): Posts
    {
        $post = new Posts();

        $success = $post->setAuthor($userId)->assign($parameters)->save();
        if ( ! $success) {
            $context = [];
            foreach ($post->getMessages() as $message) {
                $context[] = [
                    $message->getCode(),
                    $message->getField(),
                    $message->getMessage(),
                    $message->getMetaData(),
                ];
            }

            /** @var Logger $logger */
            $logger = $this->container->getShared('logger');
            $logger->error(
                'Error while creating Post!',
                $context
            );
        }

        return $post->refresh();
    }

    public function get(array $parameters)
    {
        $limitClause = $this->buildLimitClause($parameters);

        $phql
            = "SELECT posts.* FROM App\Entities\Posts as posts LEFT JOIN App\Entities\Users as users ON users.id = posts.author WHERE users.is_deleted = 0 ORDER BY created_at DESC $limitClause ";
        $query = new Query(
            $phql,
            $this->container
        );

        /** @var Simple $posts */
        return $query->execute($parameters);
    }

    private function buildLimitClause(array $parameters): string
    {
        $limit = intval($parameters['limit']);
        $page = intval($parameters['page']);

        return " LIMIT $limit OFFSET ".(($page - 1) * $limit);
    }

    public function getByUser(int $userId, array $parameters)
    {
        $limitClause = $this->buildLimitClause($parameters);

        $phql
            = "SELECT posts.* FROM App\Entities\Posts as posts LEFT JOIN App\Entities\Users as users ON users.id = posts.author WHERE posts.author = $userId AND users.is_deleted = 0 ORDER BY created_at DESC $limitClause ";
        $query = new Query(
            $phql,
            $this->container
        );

        /** @var Simple $posts */
        return $query->execute($parameters);
    }

    public function getByGroup(int $groupId, array $parameters)
    {
        $limitClause = $this->buildLimitClause($parameters);

        $phql
            = "SELECT posts.* FROM App\Entities\Posts as posts LEFT JOIN App\Entities\Users as users ON users.id = posts.author WHERE posts.group_id = $groupId AND users.is_deleted = 0 ORDER BY created_at DESC $limitClause ";
        $query = new Query(
            $phql,
            $this->container
        );

        /** @var Simple $posts */
        return $query->execute($parameters);
    }

    public function update(int $postId, array $parameters)
    {
        /** @var Posts $post */
        $post = Posts::find($postId);

        $post->assign($parameters)->save();

        return $post->refresh();
    }

    private function buildWhereClause(array $parameters): string
    {
        $clause = "";

        foreach ($parameters as $key => $value) {
            $clause .= " $key = :$key:";
        }

        return $clause;
    }
}
