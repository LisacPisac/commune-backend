<?php

namespace App\Repositories;

use App\Entities\Chats;
use Phalcon\Di\DiInterface;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Model\ResultsetInterface;

class ChatRepository
{
    private DiInterface $container;

    public function __construct()
    {
        $this->container = FactoryDefault::getDefault();
    }

    public function find(int $authUserId, int $contactUserId): ResultsetInterface
    {
        return Chats::query()
            ->where("JSON_CONTAINS(users, '$authUserId', '$') = 1")
            ->andWhere("JSON_CONTAINS(users, '$contactUserId', '$') = 1")
            ->limit(1)
            ->execute();
    }

    public function findById(int $chatId): ResultsetInterface
    {
        return Chats::find("id = $chatId");
    }

    public function findByUser(int $authUserId): ResultsetInterface
    {
        return Chats::query()
            ->where("JSON_CONTAINS(users, '$authUserId', '$') = 1")
            ->limit(1)
            ->execute();
    }

    public function index(int $userId)
    {
        return Chats::query()
            ->where("JSON_CONTAINS(users, '$userId', '$') = 1")
            ->orderBy('created_at DESC, updated_at DESC')
            ->execute();
    }

    public function create(int $authUserId, int $contactUserId)
    {
        $chat = new Chats();
        $chat->setUsers("[$authUserId, $contactUserId]")->save();

        return $chat->refresh();
    }
}
