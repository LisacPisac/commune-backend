<?php

namespace App\Repositories;

use App\Entities\Contacts;

class ContactRepository
{
    public function getForUser(int $userId): array
    {
        $contactModels = Contacts::find("(user_a = $userId OR user_b = $userId) AND pending = 0");
        $contacts = [];
        foreach ($contactModels as $contact) {
            if (intval($contact->getUserB()) !== $userId) {
                $user = $contact->getUserBEntity();
                if ( ! $user->getIsDeleted()) {
                    $contacts[] = $user;
                }
            } else {
                if (intval($contact->getUserA()) !== $userId) {
                    $user = $contact->getUserAEntity();
                    if ( ! $user->getIsDeleted()) {
                        $contacts[] = $user;
                    }
                }
            }
        }

        return $contacts;
    }

    public function getPendingForUser(int $userId)
    {
        return Contacts::find("(user_a = $userId OR user_b = $userId) AND pending = 1");
    }

    public function getPendingCountForUser(int $userId): int
    {
        return Contacts::count(
            "(user_a = $userId OR user_b = $userId) AND pending = 1"
        );
    }

    public function create(array $parameters)
    {
        $contact = new Contacts();

        $contact->assign($parameters)->save();

        return $contact->refresh();
    }

    public function update(Contacts $contact, bool $pending)
    {
        $contact->setPending($pending ? 1 : 0)->save();

        return $contact->refresh();
    }

    public function delete(int $contactA, int $contactB)
    {
        $contact = $this->find($contactA, $contactB);

        return $contact->delete();
    }

    public function find(int $contactA, int $contactB): ?Contacts
    {
        // Contacts are bidirectional
        return Contacts::findFirst(
            "(user_a = $contactA AND user_b = $contactB) OR (user_a = $contactB AND user_b = $contactA)"
        );
    }
}
