<?php

namespace App\Repositories;

use App\Entities\Messages;
use Monolog\Logger;
use Phalcon\Di\DiInterface;
use Phalcon\Di\FactoryDefault;

class MessageRepository
{
    private DiInterface $container;

    public function __construct()
    {
        $this->container = FactoryDefault::getDefault();
    }

    public function index(int $authUserId, int $chatId, int $page)
    {
        return Messages::query()
            ->where("chat_id = $chatId")
            ->limit(20, ($page - 1) * 20)
            ->orderBy('created_at DESC')
            ->execute();
    }

    public function create(int $authUserId, int $chatId, string $text)
    {
        $message = new Messages();
        $success = $message->setText($text)->setChatId($chatId)->setUserId($authUserId)->save();

        if ( ! $success) {
            $context = [];
            foreach ($message->getMessages() as $errorMessage) {
                $context[] = [
                    $errorMessage->getCode(),
                    $errorMessage->getField(),
                    $errorMessage->getMessage(),
                    $errorMessage->getMetaData(),
                ];
            }

            /** @var Logger $logger */
            $logger = $this->container->getShared('logger');
            $logger->error(
                'Error while creating Message!',
                $context
            );
        }

        return $message->refresh();
    }
}
