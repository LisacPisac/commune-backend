<?php

namespace App\Repositories;

use App\Entities\Verifications;

class VerificationRepository
{
    public function create(int $userId): Verifications
    {
        Verifications::find("user_id = $userId")->delete();

        $verification = new Verifications();
        $verification
            ->setUserId($userId)
            ->setToken(sha1(random_bytes(32)))
            ->save();

        return $verification->refresh();
    }

    public function delete(string $token)
    {
        return $this->findByToken($token)->delete();
    }

    public function findByToken(string $token)
    {
        return Verifications::findFirst("token = '$token'");
    }
}
