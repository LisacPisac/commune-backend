<?php

namespace App\Repositories;

use App\Entities\Likes;

class LikeRepository
{
    public function find(int $userId, int $postId)
    {
        return Likes::find("user_id = $userId AND post_id = $postId");
    }

    public function create(int $postId, int $userId)
    {
        $like = new Likes();

        $like->setUserId($userId)->setPostId($postId)->save();

        return $like->refresh();
    }

    public function delete(int $postId, int $userId): bool
    {
        return $this->find($userId, $postId)->delete();
    }
}
