<?php

namespace App\Repositories;

use App\Entities\Users;
use Monolog\Logger;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Model\ResultsetInterface;
use Throwable;

class UserRepository
{
    public function findById(int $id): ?Users
    {
        return Users::findFirst(
            "id = $id",
        );
    }

    public function findFirst(array $params): ?Users
    {
        $conditions = "";

        foreach ($params as $key => $value) {
            $conditions .= (($value === $params[array_key_first($params)]) ? ''
                    : ' AND ')."$key = :$key: ";
        }

        return Users::findFirst(
            [
                'conditions' => $conditions,
                'bind'       => $params,
            ]
        );
    }

    public function findByEmail(string $email): ?Users
    {
        return Users::findFirst("email = '$email'");
    }

    public function findByParameters(array $parameters): ResultsetInterface
    {
        $builder = Users::query();
        $first = true;
        foreach ($parameters as $key => $value) {
            if ($first) {
                $builder->where("$key = :$key:");
                $first = false;
            } else {
                $builder->andWhere("$key = :$key:");
            }
        }
        return $builder->bind($parameters)->execute();
    }

    public function search(string $searchTerm): ResultsetInterface
    {
        return Users::query(FactoryDefault::getDefault())
            ->where("username LIKE '%$searchTerm%'")
            ->orWhere("email LIKE '%$searchTerm%'")
            ->andWhere('is_deleted = 0')
            ->orderBy('username ASC, email ASC')
            ->execute();
    }

    public function register(
        array $params
    ): Users {
        $container = FactoryDefault::getDefault();
        $user = new Users();

        $user->setEmail($params['email']);
        $user->setUsername($params['username']);
        $user->setPassword($params['password']);
        $user->setEnabled(0);

        $success = $user->save();
        if ( ! $success) {
            $context = [];
            foreach ($user->getMessages() as $message) {
                $context[] = [
                    $message->getCode(),
                    $message->getField(),
                    $message->getMessage(),
                    $message->getMetaData(),
                ];
            }

            /** @var Logger $logger */
            $logger = $container->getShared('logger');
            $logger->error(
                'Error while creating User!',
                $context
            );
        }

        return $user->refresh();
    }

    public function verify(int $userId)
    {
        $user = $this->findById($userId);

        $user->setEnabled(1)->save();

        return $user->refresh();
    }

    public function update(Users $user, array $parameters): Users
    {
        if ( ! empty($parameters['email']) && $parameters['email'] !== $user->getEmail()) {
            $user->setEnabled(0);
        }

        $user->assign($parameters)->save();

        return $user->refresh();
    }

    public function delete(Users $user)
    {
        $container = FactoryDefault::getDefault();

        try {
            $this->update($user, ['is_deleted' => 1, 'email' => "*".random_bytes(16)."*".$user->getEmail()]);
        } catch (Throwable $throwable) {
            $context = [];
            foreach ($user->getMessages() as $message) {
                $context[] = [
                    $message->getCode(),
                    $message->getField(),
                    $message->getMessage(),
                    $message->getMetaData(),
                ];
            }

            /** @var Logger $logger */
            $logger = $container->getShared('logger');
            $logger->error(
                'Error while deleting User!',
                $context
            );

            throw $throwable;
        }

        return true;
    }
}
