<?php

namespace App\Repositories;

use App\Entities\Orders;
use App\Entities\Users;
use App\Exceptions\NotFoundException;
use App\Exceptions\ValidationException;
use App\Models\Billing\PaypalCreateOrderResponse;
use Phalcon\Messages\Message;
use Phalcon\Messages\Messages;

class OrderRepository
{
    public function create(Users $authUser, PaypalCreateOrderResponse $createOrderResponse)
    {
        Orders::find(
            "user_id = {$authUser->getId()} AND status = '".Orders::STATUS_CREATED."'"
        )->delete();

        $order = new Orders();

        $order
            ->setOrderId($createOrderResponse->id)
            ->setUserId($authUser->getId())
            ->setStatus($createOrderResponse->status)
            ->save();

        return $order->refresh();
    }

    public function capture(Users $authUser)
    {
        /** @var Orders $order */
        $order = $this->findByUser($authUser);
        if (empty($order)) {
            throw new NotFoundException();
        }

        if ($order->getStatus() === Orders::STATUS_APPROVED) {
            $messages = new Messages();
            $messages->appendMessage(new Message('Already approved', 'order'));
            throw new ValidationException($messages);
        }

        $order->setStatus(Orders::STATUS_APPROVED)->save();

        return $order->refresh();
    }

    public function findByUser(Users $authUser)
    {
        $userId = $authUser->getId();
        return Orders::findFirst(
            "user_id = $userId"
        );
    }

    public function delete(Users $authUser)
    {
        return $this->findByUser($authUser)->delete();
    }
}
