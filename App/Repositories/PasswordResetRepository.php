<?php

namespace App\Repositories;

use App\Entities\PasswordResets;
use Phalcon\Di\DiInterface;
use Phalcon\Di\FactoryDefault;

class PasswordResetRepository
{
    private DiInterface $container;

    public function __construct()
    {
        $this->container = FactoryDefault::getDefault();
    }

    public function find(int $userId): ?PasswordResets
    {
        return PasswordResets::find("user_id = $userId")->getFirst();
    }

    public function findByToken(string $token): ?PasswordResets
    {
        return PasswordResets::findFirst([
            'token = :token:',
            'bind' => [
                'token' => $token,
            ],
        ]);
    }

    public function create(int $userId): PasswordResets
    {
        $this->delete($userId);
        $passwordReset = new PasswordResets();
        $passwordReset->setUserId($userId);
        $passwordReset->setToken(sha1(random_bytes(32)));
        $passwordReset->save();

        return $passwordReset->refresh();
    }

    public function delete(int $userId)
    {
        return PasswordResets::find("user_id = $userId")->delete();
    }
}
