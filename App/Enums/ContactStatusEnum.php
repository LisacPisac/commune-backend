<?php

namespace App\Enums;

class ContactStatusEnum
{
    const NOT_CONTACT = 0;
    const PENDING = 1;
    const CONTACTS = 2;
}

