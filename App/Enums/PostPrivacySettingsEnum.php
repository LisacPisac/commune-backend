<?php

namespace App\Enums;

class PostPrivacySettingsEnum
{
    const EVERYONE = 'everyone';
    const CONTACTS = 'contacts';
    const GROUP = 'group';
}