<?php

namespace App\Enums;

class DbTableNamesEnum
{
    public const ALL_TABLES
        = [
            self::USERS,
            self::GROUPS,
            self::POSTS,
            self::COMMENTS,
            self::GROUP_POSTS,
            self::GROUP_MEMBERS,
            self::PASSWORD_RESETS,
            self::SESSIONS,
            self::FILES,
            self::POST_FILES,
            self::LIKES,
            self::CONTACTS,
            self::MEMBERS,
            self::CHATS,
            self::MESSAGES,
            self::ORDERS,
            self::VERIFICATIONS,
        ];

    public const USERS = 'users';
    public const GROUPS = 'groups';
    public const POSTS = 'posts';
    public const COMMENTS = 'comments';
    public const GROUP_POSTS = 'group_posts';
    public const GROUP_MEMBERS = 'group_members';
    public const PASSWORD_RESETS = 'password_resets';
    public const SESSIONS = 'sessions';
    public const FILES = 'files';
    public const POST_FILES = 'post_files';
    public const LIKES = 'likes';
    public const CONTACTS = 'contacts';
    public const MEMBERS = 'members';
    public const CHATS = 'chats';
    public const MESSAGES = 'messages';
    public const ORDERS = 'orders';
    public const VERIFICATIONS = 'verifications';
}
