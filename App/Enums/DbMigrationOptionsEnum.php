<?php

namespace App\Enums;

/**
 * Class DbMigrationOptionsEnum
 *
 * Class containing Enums for commonly used column option sets
 */
class DbMigrationOptionsEnum
{
    public const PRIMARY_KEY
        = [
            'signed' => false,
        ];

    public const FOREIGN_KEY
        = [
            'signed' => false,
        ];

    public const STRING_255_NULLABLE
        = [
            'null'  => true,
            'limit' => 255,
        ];

    public const STRING_255_NON_NULLABLE
        = [
            'null'  => false,
            'limit' => 255,
        ];

    public const NULLABLE
        = [
            'null' => true,
        ];

    public const NON_NULLABLE
        = [
            'null' => false,
        ];

    public const DEFAULT_TRUE
        = [
            'default' => true,
        ];

    public const DEFAULT_FALSE
        = [
            'default' => false,
        ];

    public const FK_CASCADE = ['delete' => 'CASCADE', 'update' => 'CASCADE'];

    public const FK_SET_NULL = ['delete' => 'SET_NULL', 'update' => 'NO_ACTION'];
}
