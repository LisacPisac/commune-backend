<?php

namespace App\Enums;

class GlobalServicesEnum
{
    public const CRYPT = 'crypt';
    public const DB = 'db';
    public const ENV = 'env';
    public const URL = 'url';
    public const SESSION = 'session';
    public const LOGGER = 'logger';
    public const EMAIL = 'email';
    public const SECURITY = 'security';
}
