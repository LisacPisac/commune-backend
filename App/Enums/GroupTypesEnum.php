<?php

namespace App\Enums;

class GroupTypesEnum
{
    public const ALL_TYPES
        = [
            self::TEXT,
            self::AUDIO,
            self::IMAGE,
            self::VIDEO,
            self::MIXED,
        ];

    public const TEXT = 'text';
    public const AUDIO = 'audio';
    public const IMAGE = 'image';
    public const VIDEO = 'video';
    public const MIXED = 'mixed';
}
