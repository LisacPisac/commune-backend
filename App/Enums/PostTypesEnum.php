<?php

namespace App\Enums;

class PostTypesEnum
{
    public const ALL_TYPES
        = [
            self::TEXT,
            self::AUDIO,
            self::VIDEO,
        ];

    public const TEXT = 'text';
    public const AUDIO = 'audio';
    public const VIDEO = 'video';
}
