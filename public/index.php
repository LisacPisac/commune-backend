<?php
declare(strict_types=1);

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Model\Binder;

error_reporting(0);

define('BASE_PATH', dirname(__DIR__));
const APP_PATH = BASE_PATH . '/App';

require_once(BASE_PATH . '/vendor/autoload.php');

try {
    /**
     * The FactoryDefault Dependency Injector automatically registers the services that
     * provide a full stack framework. These default services can be overidden with custom ones.
     */
    $container = new FactoryDefault();

    /**
     * Autoload namespaces and directories
     */
    include APP_PATH . '/Config/loader.php';

    /**
     * Include Services
     */
    include APP_PATH.'/Config/services.php';
    include APP_PATH.'/Config/services_backend.php';

    /**
     * Start the application
     */
    $app = new Micro($container);

    /**
     * Include additional App bootstrapping
     */
    include APP_PATH . '/app.php';

    $container->setShared('application', $app);

    $app->setModelBinder(new Binder());

    /**
     * Handle the request
     */
    $app->handle($_SERVER['REQUEST_URI']);
} catch (Throwable $e) {
    /** @var \Monolog\Logger $logger */
    $logger = $container->getShared('logger');
    $logger->error($e->getMessage());
}
