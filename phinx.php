<?php

use Dotenv\Dotenv;

require_once 'vendor/autoload.php';

Dotenv::createImmutable(__DIR__)->load();

return
    [
        'paths' => [
            'migrations' => '%%PHINX_CONFIG_DIR%%/App/Storage/Migrations',
            'seeds' => '%%PHINX_CONFIG_DIR%%/App/Storage/Seeds',
            'bootstrap' => './phinx-bootstrap.php'
        ],
        'environments' => [
            'default_migration_table' => 'phinxlog',
            'default_environment' => 'development',
            'production' => [
                'adapter' => 'mysql',
                'host' => 'localhost',
                'name' => 'production_db',
                'user' => 'root',
                'pass' => '',
                'port' => '3306',
                'charset' => 'utf8',
            ],
            'development' => [
                'adapter' => strtolower($_ENV['DB_TYPE']),
                'host' => $_ENV['DB_HOST'],
                'name' => $_ENV['DB_NAME'],
                'user' => $_ENV['DB_USER'],
                'pass' => $_ENV['DB_PASS'],
                'port' => '3306',
                'charset' => 'utf8',
            ],
            'testing' => [
                'adapter' => 'mysql',
                'host' => 'localhost',
                'name' => 'testing_db',
                'user' => 'root',
                'pass' => '',
                'port' => '3306',
                'charset' => 'utf8',
            ]
        ],
        //        'version_order' => 'creation'
        'version_order' => 'execution'
    ];
