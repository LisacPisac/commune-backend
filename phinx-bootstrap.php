<?php

/**
 * @var string $filename The file name as provided by the configuration
 * @var string $filePath The absolute, real path to the file
 * @var \Symfony\Component\Console\Input\InputInterface $input The executing command's input object
 * @var \Symfony\Component\Console\Output\OutputInterface $output The executing command's output object
 * @var \Phinx\Console\Command\AbstractCommand $context the executing command object
 */

function dbAutoloader($className)
{
    include_once(__DIR__ . "/" . str_replace('\\', '/', $className) . ".php");
}
spl_autoload_register("dbAutoloader");

require_once ('./vendor/autoload.php');
