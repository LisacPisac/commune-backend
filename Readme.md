# Dependencies

- PSR
- PhalconPHP
- Imagick

# Setup

- Run `composer install`
- Initialize phinx with `php vendor/bin/phinx init`
-

# Configure

## Mysql

- Set max packet size to 1GB (1073741824)
- 
